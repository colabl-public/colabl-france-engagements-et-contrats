#Summary 

- [Avertissement Solennel contre la Reproduction Illicite des Politiques de Confidentialité](#section-id-5)
- [Préambule](#section-id-20)
  - [1. Introduction et portée de la politique](#section-id-22)
  - [2. Identification de l'entité responsable du traitement des données (le Responsable du traitement)](#section-id-37)
  - [3. Cadre juridique applicable (RGPD, lois locales sur la protection des données)](#section-id-59)
    - [3.1. Règlement Général sur la Protection des Données (RGPD)](#section-id-63)
    - [3.2. Lois nationales relatives à la Protection des Données](#section-id-67)
    - [3.3. Principes de Conformité](#section-id-71)
    - [3.4 Droits des Personnes Concernées](#section-id-81)
- [Définitions](#section-id-89)
  - [4. Définition des termes clés : données personnelles, traitement, consentement, etc.](#section-id-90)
    - [4.1. Données à Caractère Personnel](#section-id-94)
    - [4.2. Traitement](#section-id-98)
    - [4.3. Consentement de la Personne Concernée](#section-id-102)
    - [4.4. Responsable du Traitement](#section-id-106)
    - [4.5. Sous-traitant](#section-id-110)
    - [4.6. Tiers](#section-id-114)
    - [4.7. Violation de Données à Caractère Personnel](#section-id-118)
- [Champ d'Application](#section-id-124)
  - [5. Précision des services, logiciels et applications mobiles couverts par la politique](#section-id-126)
    - [5.1. Services, Logiciels et Applications Mobiles Concernés](#section-id-128)
    - [5.2. Inclusion et Exclusions](#section-id-138)
    - [5.3. Modifications du champ d'application](#section-id-144)
  - [6. Distinction entre les dispositions communes et les spécificités par service](#section-id-151)
    - [6.1. Dispositions communes](#section-id-155)
    - [6.2. Spécificités par service](#section-id-165)
- [Principes Généraux de Protection des Données](#section-id-181)
  - [7. Légalité, loyauté et transparence](#section-id-183)
    - [7.1. Principe de Légalité](#section-id-187)
    - [7.2. Principe de Loyauté](#section-id-191)
    - [7.3. Principe de Transparence](#section-id-195)
  - [8. Limitation des Finalités](#section-id-201)
    - [8.1. Définition du Principe](#section-id-205)
    - [8.2. Application du Principe](#section-id-209)
    - [8.3. Transparence des Finalités](#section-id-217)
  - [9. Minimisation des données](#section-id-223)
    - [9.1. Définition du principe](#section-id-228)
    - [9.2. Mise en Œuvre du principe](#section-id-233)
    - [9.3. Application pratique](#section-id-242)
    - [9.4. Transparence et Responsabilité](#section-id-248)
  - [10. Exactitude](#section-id-254)
    - [10.1. Définition du principe](#section-id-258)
    - [10.2. Mise en œuvre du principe](#section-id-262)
    - [10.3. Responsabilités et Engagements](#section-id-270)
    - [10.4. Transparence et Information](#section-id-277)
  - [11. Limitation de la conservation](#section-id-284)
    - [11.1. Définition du Principe](#section-id-289)
    - [11.2. Application du Principe](#section-id-293)
    - [11.3. Mesures de Mise en Œuvre](#section-id-301)
    - [11.4. Transparence envers les Personnes Concernées](#section-id-307)
  - [12. Intégrité et confidentialité](#section-id-313)
    - [12.1. Définition du principe](#section-id-319)
    - [12.2. Mise en œuvre des principes](#section-id-323)
    - [12.3. Engagement du Responsable du traitement](#section-id-330)
    - [12.3. Transparence et Assurance](#section-id-335)
- [Collecte des Données Personnelles](#section-id-341)
  - [13. Types de données collectées](#section-id-343)
    - [13.1. Informations communes à tous les services](#section-id-347)
    - [13.2. Données spécifiques selon le service ou l'application](#section-id-357)
  - [14. Bases juridiques du traitement (consentement, exécution d'un contrat, obligations légales, intérêts légitimes)](#section-id-371)
    - [14.1. Consentement](#section-id-375)
    - [14.2. Exécution d'un Contrat](#section-id-379)
    - [14.3. Obligations Légales](#section-id-383)
    - [14.4. Intérêts Légitimes](#section-id-387)
  - [15. Modalités de collecte (directe/indirecte)](#section-id-396)
    - [15.1. Collecte Directe](#section-id-400)
    - [15.2. Collecte Indirecte](#section-id-410)
- [Utilisation des Données](#section-id-421)
  - [16. Finalités générales du traitement](#section-id-423)
    - [16.1. Fourniture et gestion des services](#section-id-428)
    - [16.2. Transactions commerciales](#section-id-432)
    - [16.3. Communication avec les utilisateurs](#section-id-436)
    - [16.4. Amélioration des services et développement de nouveaux produits](#section-id-440)
    - [16.5. Sécurité et conformité légale](#section-id-444)
  - [17. Finalités](#section-id-450)
    - [17.1. Services de Localisation](#section-id-454)
    - [17.2. Services de santé et de bien-être](#section-id-458)
    - [17.3. Services d'analyse comportementale](#section-id-462)
    - [17.4. Services de support et de Communication interactifs](#section-id-466)
    - [17.5. Participation à des Événements ou à des programmes de fidélité](#section-id-470)
  - [18. Profilage et décision automatique, le cas échéant](#section-id-476)
    - [18.1. Définition et Application](#section-id-480)
      - [18.1.1 Consentement et Droits des personnes concernées](#section-id-486)
    - [18.1.2. Garanties et Mesures de sécurité](#section-id-493)
- [Partage et Transfert des données](#section-id-501)
  - [19. Transferts internes (entre différents services de l'entité)](#section-id-503)
    - [19.1. Cadre juridique et organisationnel](#section-id-507)
    - [19.2. Mesures de Sécurité et de confidentialité](#section-id-514)
    - [19.3. Responsabilité et conformité](#section-id-520)
  - [20. Divulgation à des tiers (sous-traitants, partenaires commerciaux)](#section-id-526)
    - [20.1. Responsabilité](#section-id-528)
    - [20.2. Cadre contractuel et obligations des tiers](#section-id-532)
    - [20.3. Transparence et Consentement des personnes concernées](#section-id-538)
    - [20.4. Transferts Internationaux](#section-id-543)
  - [21. Transferts internationaux de données (garanties et mécanismes de protection)](#section-id-549)
    - [21.1. Principes généraux](#section-id-553)
    - [21.2. Garanties et Mécanismes de protection](#section-id-557)
    - [21.3. Droits des Personnes Concernées et Recours](#section-id-563)
    - [21.4. Évaluation et Réévaluation des Transferts](#section-id-567)
- [Droits des Utilisateurs](#section-id-573)
  - [22. Détail des droits accordés par le RGPD (accès, rectification, suppression, limitation, portabilité, opposition)](#section-id-575)
    - [22.1. Droit d'Accès](#section-id-579)
    - [22.2. Droit de Rectification](#section-id-583)
    - [22.3. Droit à l'Effacement (« Droit à l'Oubli »)](#section-id-587)
    - [22.4. Droit à la Limitation du Traitement](#section-id-591)
    - [22.5. Droit à la Portabilité des Données](#section-id-595)
    - [22.6. Droit d'Opposition](#section-id-599)
  - [23. Modalités d'exercice de ces droits](#section-id-605)
    - [23.1. Procédures de demande](#section-id-609)
    - [23.2. Délais de traitement](#section-id-615)
    - [23.3. Coût de la demande](#section-id-619)
    - [23.4. Refus de satisfaction aux demandes](#section-id-623)
  - [24. Contact du délégué à la protection des données (DPO)](#section-id-629)
    - [24.1. Coordonnées du DPO](#section-id-635)
    - [24.2. Rôle et Responsabilités du DPO](#section-id-647)
- [Sécurité des Données](#section-id-658)
  - [25. Mesures techniques et organisationnelles mises en place](#section-id-660)
    - [25.1. Mesures techniques](#section-id-664)
    - [25.2. Mesures organisationnelles](#section-id-672)
  - [26. Gestion des violations de données](#section-id-681)
    - [26.1. Détection et Évaluation des violations](#section-id-685)
    - [26.2. Notification des Violations](#section-id-690)
    - [26.3. Mesures correctives et préventives](#section-id-695)
    - [26.4. Documentation et Revue](#section-id-701)
  - [27. Cookies et Technologies Similaires](#section-id-707)
    - [27.1. Utilisation de cookies et technologies similaires](#section-id-711)
    - [27.2. Finalités du suivi](#section-id-715)
    - [27.3. Gestion du consentement pour les cookies](#section-id-719)
- [Modifications de la Politique de Confidentialité](#section-id-727)
  - [28. Processus de mise à jour](#section-id-729)
    - [28.1. Engagement de Notification](#section-id-733)
    - [28.2. Contenu des Modifications](#section-id-737)
    - [28.3. Accès à la Politique Révisée](#section-id-741)
    - [28.4. Droits des Utilisateurs](#section-id-745)
  - [29. Notification des modifications](#section-id-751)
    - [29.1. Procédure de Notification](#section-id-755)
    - [29.2. Droits des Utilisateurs en cas de modification](#section-id-762)
- [Dispositions Finales](#section-id-768)
  - [30. Juridiction compétente et Loi applicable](#section-id-770)
    - [30.1. Loi Applicable](#section-id-774)
    - [30.2. Juridiction compétente](#section-id-778)
    - [30.3. Recours et Voies de droit](#section-id-782)
  - [31. Voies de recours en cas de litige](#section-id-788)
    - [31.1. Recours Administratif](#section-id-792)
    - [31.2. Recours Judiciaire](#section-id-797)
    - [31.3. Mécanismes de Médiation et d'Arbitrage](#section-id-802)
- [Annexes](#section-id-808)
  - [32. Glossaire des Termes Techniques et Juridiques](#section-id-810)
  - [33. Modèles de formulaire d'exercice des droits par les utilisateurs](#section-id-825)
    - [33.1. Modèle de Formulaire pour le Droit d'Accès](#section-id-829)
    - [33.2. Modèle de Formulaire pour le Droit de Rectification](#section-id-854)
  


Date de dernière mise à jour : 29/02/2024


 
<div id='section-id-5'/>

# Avertissement Solennel contre la Reproduction Illicite des Politiques de Confidentialité

> Dans le cadre de la stricte observance des normes juridiques régissant la propriété intellectuelle et le respect des droits d'auteur, il est impératif d'adresser une mise en garde solennelle à l'encontre de toute pratique consistant à s'approprier indûment ou à reproduire sans autorisation les politiques de confidentialité développées et publiées sur notre site Internet. Ces documents, fruits d'une élaboration méticuleuse et d'un investissement intellectuel conséquent, constituent des œuvres protégées au titre du droit d'auteur, conformément aux dispositions du Code de la propriété intellectuelle.
>
> Il est essentiel de comprendre que toute tentative de reproduction, de copie ou d'utilisation de ces contenus sans le consentement explicite du titulaire des droits expose le contrevenant à des risques juridiques sérieux, incluant des poursuites pour parasitisme économique. Cette qualification juridique s'applique à la situation où un individu ou une entité, dans une démarche lucrative et injustifiée, se livre à l'imitation ou à l'exploitation d'une valeur économique propre à autrui, caractérisée par un savoir-faire distinctif, un travail intellectuel et des investissements spécifiques, procurant ainsi un avantage concurrentiel indû.
> 
> À titre illustratif, le cas de la société KALYPSO, qui s'était appropriée les conditions générales de vente de la société VENTE-PRIVEE.COM pour les utiliser dans un cadre concurrentiel, illustre parfaitement les conséquences de telles pratiques. La Cour d'appel de Paris, par son arrêt du [24 septembre 2008 (n°07/03336)](https://www.doctrine.fr/d/CA/Paris/2008/SK802B77FC5E6A14FCFF36), a non seulement reconnu le caractère parasitaire de l'acte mais a également condamné la société KALYPSO à verser des dommages et intérêts s'élevant à 10.000 euros, outre les frais judiciaires associés à la défense de ses intérêts et ceux de la partie lésée.
>
> Dans cette optique, nous tenons à souligner que toute violation avérée des droits d'auteur relatifs à nos politiques de confidentialité entraînera l'initiation de procédures judiciaires rigoureuses, visant à la réparation intégrale du préjudice subi. Cette démarche inclura la demande de dommages et intérêts pour parasitisme économique, ainsi que la couverture des frais de justice et d'avocat.
>
> Afin d'éviter de telles situations préjudiciables pour toutes les parties concernées, nous invitons instamment à la création de contenus originaux ou à solliciter l'autorisation préalable pour toute utilisation de matériaux protégés. Le respect des dispositions légales en matière de droit d'auteur est impératif pour garantir une concurrence loyale et équitable, ainsi que pour promouvoir l'innovation et la créativité dans le respect des droits de chacun.
>
> Nous demeurons vigilants et déterminés à protéger nos droits intellectuels et n'hésiterons pas à prendre toutes les mesures nécessaires pour préserver l'intégrité de nos travaux et de nos investissements.


<div id='section-id-20'/>

# Préambule

<div id='section-id-22'/>

## 1. Introduction et portée de la politique

La présente politique de confidentialité et de protection des données personnelles (ci-après la "Politique") établit les principes et directives mis en œuvre par Colabl France (ci-après le "Responsable du traitement"), société inscrite au Registre du Commerce et des Sociétés sous le numéro 953 740 743, ayant son siège social au 83 A, rue des Alliés, 42100 Saint-Etienne, dans le cadre de ses activités commerciales et opérationnelles.

Cette Politique est conçue pour assurer la protection des données personnelles (ci-après les "Données") que le Responsable du traitement collecte, traite, et conserve en relation avec la fourniture de ses services, logiciels, et applications mobiles (ci-après collectivement désignés les "Services"). Elle vise à garantir le respect de la confidentialité et de la sécurité des Données des utilisateurs (ci-après les "Utilisateurs") conformément au [Règlement Général sur la Protection des Données (RGPD) (UE) 2016/679](https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32016R0679) du Parlement européen et du Conseil du 27 avril 2016, ainsi qu'aux lois nationales applicables en matière de protection des données personnelles.

La Politique s'applique sans restriction ni réserve à tous les Services proposés par le Responsable du traitement, y compris mais sans s'y limiter, à toute interaction des Utilisateurs avec lesdits Services, que ce soit via des sites internet, des applications mobiles, ou tout autre moyen technologique permettant l'accès aux Services. Elle couvre également les traitements spécifiques de Données réalisés dans le cadre de Services particuliers, lesquels sont détaillés dans les sections subséquentes de la présente Politique.

La Politique explicite les types de Données collectées, les finalités pour lesquelles ces Données sont traitées, les droits des Utilisateurs en matière de protection de leurs Données personnelles, ainsi que les mesures mises en place pour préserver la sécurité et la confidentialité des Données.

Il est impératif que les Utilisateurs prennent connaissance de la présente Politique afin de comprendre les pratiques du Responsable du traitement relatives au traitement de leurs Données personnelles et la manière dont ils peuvent exercer leurs droits. L'utilisation des Services par les Utilisateurs constitue l'acceptation pleine et entière des termes de la présente Politique.

Le Responsable du traitement se réserve le droit de modifier la présente Politique à tout moment, en fonction de l'évolution des lois et réglementations applicables en matière de protection des données personnelles ou des modifications apportées aux Services. Les Utilisateurs seront informés de toute modification substantielle, et la date de dernière mise à jour figurera en tête de la présente Politique.


<div id='section-id-37'/>

## 2. Identification de l'entité responsable du traitement des données (le "Responsable du traitement")

Conformément aux dispositions du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "RGPD"), ainsi qu'aux lois nationales applicables en matière de protection des données personnelles, l'entité ci-après désignée assume la qualité de Responsable du Traitement des données personnelles collectées et traitées dans le cadre de l'utilisation des services, logiciels et applications mobiles mentionnés dans la présente politique de confidentialité et de protection des données personnelles.

**Dénomination et Coordonnées du Responsable du Traitement :**

> **Raison sociale :** Colabl France \
> **Forme juridique :** SAS  \
> **Siège social :** [ADRESSE_POSTALE_SIEGE_COLABL] \
> **Représentée par :** [DIRECTEUR_PUBLICATION_COLABL] \
> **Inscrite au Registre du Commerce et des Sociétés sous le numéro :** 953 740 743 \
> **Numéro de TVA intracommunautaire :** [N_TVA_COLABL] \
> **Numéro de téléphone :** [NUMERO_TEL_STANDARD_COLABL] \
> **Adresse électronique :** [EMAIL_RGPD_COLABL]

Cette entité, en sa qualité de Responsable du Traitement, s'engage à respecter le cadre légal en vigueur concernant le traitement des données personnelles et garantit la mise en œuvre de mesures adéquates pour assurer la protection, la confidentialité, et la sécurité des données personnelles des utilisateurs, conformément aux dispositions légales et réglementaires susmentionnées.

Le Responsable du Traitement est chargé de déterminer les finalités et les moyens du traitement des données personnelles collectées. À cet égard, il veille à ce que toutes les procédures mises en place soient conformes au RGPD et aux législations nationales relatives à la protection des données personnelles, garantissant ainsi un niveau de protection élevé des droits et libertés des personnes physiques concernées par les traitements.

Pour toute question relative à la gestion et à la protection de vos données personnelles, ainsi que pour exercer vos droits tels que définis par le RGPD et les lois nationales, vous pouvez contacter le Responsable du Traitement aux coordonnées fournies ci-dessus.


<div id='section-id-59'/>

## 3. Cadre juridique applicable (RGPD, lois locales sur la protection des données)

La présente politique de confidentialité et de protection des données personnelles est édictée en stricte conformité avec le cadre juridique applicable en matière de traitement et de protection des données à caractère personnel, lequel comprend notamment le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné par "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi que les législations nationales pertinentes en vigueur dans les juridictions où le Responsable du Traitement opère.

<div id='section-id-63'/>

### 3.1. Règlement Général sur la Protection des Données (RGPD)

§ Le RGPD constitue le corpus législatif principal régissant le traitement des données à caractère personnel au sein de l'Union Européenne. Il établit les principes relatifs au traitement des données personnelles, confère aux personnes concernées divers droits vis-à-vis de leurs données personnelles, et impose aux Responsables du Traitement et aux Sous-traitants des obligations précises quant à la gestion et à la sécurisation de ces données. Le RGPD est directement applicable dans l'ensemble des États membres de l'Union Européenne, assurant ainsi un niveau élevé et uniforme de protection des données personnelles.

<div id='section-id-67'/>

### 3.2. Lois nationales relatives à la Protection des Données

§ En complément du RGPD, le Responsable du Traitement se conforme également aux lois et règlements locaux relatifs à la protection des données personnelles dans les juridictions où il exerce ses activités. Ces dispositions législatives nationales peuvent préciser, compléter ou renforcer les règles établies par le RGPD, notamment en matière de gestion des droits des personnes concernées, de notification des violations de données à caractère personnel, et de sanctions en cas de non-respect des obligations de protection des données.

<div id='section-id-71'/>

### 3.3. Principes de Conformité

§ Dans le cadre de son activité, le Responsable du Traitement s'engage à :

- Assurer la licéité, la loyauté et la transparence du traitement des données personnelles ;
- Limiter la collecte des données aux finalités spécifiquement déclarées, explicites et légitimes ;
- Veiller à l'exactitude des données et à leur mise à jour ;
- Limiter la conservation des données à une durée n'excédant pas celle nécessaire au regard des finalités pour lesquelles elles sont traitées ;
- Garantir l'intégrité et la confidentialité des données personnelles, en mettant en œuvre des mesures techniques et organisationnelles adéquates pour assurer un niveau de sécurité approprié au risque.

<div id='section-id-81'/>

### 3.4 Droits des Personnes Concernées

§ Conformément au RGPD et aux lois nationales applicables, les personnes dont les données personnelles sont traitées bénéficient de droits spécifiques, incluant le droit d'accès, de rectification, d'effacement, de limitation du traitement, de portabilité des données, et d'opposition.

§ Le Responsable du Traitement s'engage à faciliter l'exercice de ces droits et à fournir toutes les informations nécessaires pour permettre aux personnes concernées de les exercer efficacement.

La présente politique est régie et doit être interprétée conformément au RGPD et aux lois nationales applicables. Tout litige relatif à l'interprétation ou à l'application de cette politique sera soumis aux juridictions compétentes conformément à la législation en vigueur.

<div id='section-id-89'/>

# Définitions
<div id='section-id-90'/>

## 4. Définition des termes clés : données personnelles, traitement, consentement, etc.

Aux fins de la présente politique de confidentialité et de protection des données personnelles, les termes ci-après auront la signification qui leur est attribuée dans ce segment, conformément aux dispositions du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi que conformément aux termes généralement admis dans le cadre des législations nationales applicables en matière de protection des données personnelles.

<div id='section-id-94'/>

### 4.1. Données à Caractère Personnel

§ Toute information se rapportant à une personne physique identifiée ou identifiable; est réputée être une "personne physique identifiable" une personne physique qui peut être identifiée, directement ou indirectement, notamment par référence à un identifiant tel qu'un nom, un numéro d'identification, des données de localisation, un identifiant en ligne, ou à un ou plusieurs éléments spécifiques propres à son identité physique, physiologique, génétique, psychique, économique, culturelle ou sociale.

<div id='section-id-98'/>

### 4.2. Traitement

Toute opération ou ensemble d'opérations effectuées sur des données à caractère personnel ou sur des ensembles de données à caractère personnel, que ce soit par des moyens automatisés ou non, telles que la collecte, l'enregistrement, l'organisation, la structuration, la conservation, l'adaptation ou la modification, l'extraction, la consultation, l'utilisation, la divulgation par transmission, la diffusion ou toute autre forme de mise à disposition, le rapprochement ou l'interconnexion, la limitation, l'effacement ou la destruction.

<div id='section-id-102'/>

### 4.3. Consentement de la Personne Concernée

§ Toute manifestation de volonté libre, spécifique, éclairée et univoque par laquelle la personne concernée accepte, par une déclaration ou par un acte positif clair, que des données à caractère personnel la concernant fassent l'objet d'un traitement.

<div id='section-id-106'/>

### 4.4. Responsable du Traitement

§ La personne physique ou morale, l'autorité publique, le service ou tout autre organisme qui, seul ou conjointement avec d'autres, détermine les finalités et les moyens du traitement des données à caractère personnel; lorsque les finalités et les moyens de ce traitement sont déterminés par le droit de l'Union ou le droit des États membres, le responsable du traitement ou les critères spécifiques de sa nomination peuvent être prévus par le droit de l'Union ou par le droit des États membres.

<div id='section-id-110'/>

### 4.5. Sous-traitant

§ La personne physique ou morale, l'autorité publique, le service ou tout autre organisme qui traite des données à caractère personnel pour le compte du responsable du traitement.

<div id='section-id-114'/>

### 4.6. Tiers

Une personne physique ou morale, une autorité publique, un service ou un organisme autre que la personne concernée, le responsable du traitement, le sous-traitant et les personnes qui, placées sous l'autorité directe du responsable du traitement ou du sous-traitant, sont autorisées à traiter des données à caractère personnel.

<div id='section-id-118'/>

### 4.7. Violation de Données à Caractère Personnel

§ Une violation de la sécurité entraînant, de manière accidentelle ou illicite, la destruction, la perte, l'altération, la divulgation non autorisée de données à caractère personnel transmises, conservées ou traitées d'une autre manière, ou l'accès non autorisé à de telles données.

§ Les définitions fournies dans cette section visent à éclairer le lecteur sur les termes essentiels utilisés dans le cadre de cette politique de confidentialité et de protection des données personnelles, afin d'assurer une compréhension et une interprétation correctes des engagements et des obligations du Responsable du Traitement ainsi que des droits conférés aux personnes concernées.

<div id='section-id-124'/>

# Champ d'Application

<div id='section-id-126'/>

## 5. Précision des services, logiciels et applications mobiles couverts par la politique

<div id='section-id-128'/>

### 5.1. Services, Logiciels et Applications Mobiles Concernés

§ La présente Politique s'applique sans restriction ni réserve à l'ensemble des services, logiciels, et applications mobiles proposés par le Responsable du Traitement, y compris mais sans s'y limiter, à ceux identifiés ci-après :

- **Services en Ligne** : tout service accessible par le biais d'internet, y compris les sites web exploités sous les noms de domaine enregistrés par le Responsable du Traitement.

- **Logiciels** : tout programme informatique et solution logicielle développée, distribuée ou mise à disposition par le Responsable du Traitement, qu'il s'agisse de logiciels destinés à être installés sur les équipements informatiques des Utilisateurs ou de solutions accessibles en mode SaaS (Software as a Service).

- **Applications Mobiles** : toutes les applications développées par le Responsable du Traitement pour être utilisées sur des dispositifs mobiles, telles que smartphones et tablettes.

<div id='section-id-138'/>

### 5.2. Inclusion et Exclusions

§ La Politique couvre le traitement de toutes les données à caractère personnel recueillies ou générées par les Utilisateurs lors de l'accès et de l'utilisation des services, logiciels, et applications mobiles susmentionnés. Elle englobe également les données personnelles collectées par le biais de formulaires de contact, d'inscriptions, ou lors de la création de comptes utilisateurs.

§ Nonobstant ce qui précède, la présente Politique ne s'applique pas aux services, logiciels et applications mobiles exploités par des tiers, même si ceux-ci sont accessibles ou interconnectés via les services ou applications du Responsable du Traitement. Les Utilisateurs sont encouragés à consulter les politiques de confidentialité propres à ces services tiers pour s'informer des pratiques de traitement de leurs données personnelles.

<div id='section-id-144'/>

### 5.3. Modifications du champ d'application

§ Le Responsable du Traitement se réserve le droit de modifier, d'ajouter ou de supprimer des services, logiciels, et applications mobiles couverts par la présente Politique, à sa seule discrétion. Toute modification significative du champ d'application de la Politique sera communiquée aux Utilisateurs par tout moyen jugé approprié par le Responsable du Traitement, y compris par la publication d'une mise à jour de la Politique sur les sites web ou les plateformes d'applications concernés.

En conséquence, les Utilisateurs sont invités à consulter régulièrement la présente Politique afin de se tenir informés de tout changement pouvant affecter le traitement de leurs données personnelles par le Responsable du Traitement.


<div id='section-id-151'/>

## 6. Distinction entre les dispositions communes et les spécificités par service

La présente section de la Politique de Confidentialité et de Protection des Données Personnelles a pour objet de clarifier la distinction entre les dispositions communes applicables à l'ensemble des services, logiciels et applications mobiles proposés par Colabl France (ci-après désigné le "Responsable du Traitement"), et les spécificités propres à certains services, logiciels ou applications mobiles, qui requièrent des mesures de traitement des données à caractère personnel adaptées.

<div id='section-id-155'/>

### 6.1. Dispositions communes

§ Les dispositions communes sont celles qui s'appliquent de manière uniforme à tous les services, logiciels et applications mobiles exploités par le Responsable du Traitement. Ces dispositions englobent, sans s'y limiter, les principes suivants :

- **Principes de Protection des Données** : Respect des principes fondamentaux de protection des données à caractère personnel, tels que la licéité, la loyauté, la transparence, la limitation des finalités, la minimisation des données, l'exactitude, la limitation de la conservation, l'intégrité et la confidentialité.

- **Droits des Utilisateurs** : Reconnaissance et facilitation de l'exercice des droits des utilisateurs conformément au Règlement Général sur la Protection des Données (RGPD) et aux législations nationales applicables, incluant le droit d'accès, de rectification, d'effacement, de limitation du traitement, de portabilité des données et d'opposition.

- **Sécurité des Données** : Mise en œuvre de mesures techniques et organisationnelles appropriées pour assurer un niveau de sécurité adapté au risque lié au traitement des données à caractère personnel.

<div id='section-id-165'/>

### 6.2. Spécificités par service

§ Les spécificités par service se réfèrent aux dispositions particulières qui s'appliquent à certains services, logiciels ou applications mobiles en fonction de leur nature spécifique, des types de données à caractère personnel traitées, des finalités du traitement et des contextes d'utilisation. Ces spécificités peuvent inclure :

- **Types de données collectées** : Identification des données à caractère personnel spécifiquement collectées pour chaque service, logiciel ou application mobile, qui peuvent différer en fonction des fonctionnalités offertes et des besoins en traitement.

- **Finalités spécifiques du traitement** : Définition des finalités précises pour lesquelles les données à caractère personnel sont traitées dans le cadre d'un service, logiciel ou application mobile donné, qui peuvent varier en fonction des objectifs spécifiques de chaque offre.

- **Consentement spécifique** : Obtention d'un consentement spécifique des utilisateurs pour certaines opérations de traitement qui requièrent une approbation explicite en vertu du RGPD ou des législations nationales applicables, en fonction des caractéristiques propres au service concerné.

- **Durées de conservation déterminées** : Établissement de périodes de conservation des données à caractère personnel qui tiennent compte de la nature du service, logiciel ou application mobile, et des obligations légales ou réglementaires spécifiques.

- **Partenaires et Tiers impliqués** : Identification de tout partenaire ou tiers spécifiquement associé au traitement des données dans le cadre d'un service, logiciel ou application mobile, y compris les sous-traitants ou autres entités qui participent à la fourniture du service.

La distinction entre les dispositions communes et les spécificités par service vise à assurer une compréhension claire et précise des pratiques de traitement des données à caractère personnel mises en œuvre par le Responsable du Traitement, tout en adaptant la protection des données aux caractéristiques uniques de chaque service, logiciel ou application mobile proposé. Les utilisateurs sont encouragés à prendre connaissance des spécificités applicables aux services, logiciels ou applications mobiles qu'ils utilisent, qui sont détaillées dans les sections correspondantes de la présente Politique ou dans des annexes spécifiques.

<div id='section-id-181'/>

# Principes Généraux de Protection des Données

<div id='section-id-183'/>

## 7. Légalité, loyauté et transparence

Conformément aux exigences édictées par le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi qu'aux dispositions des législations nationales applicables en matière de protection des données personnelles, le traitement des données à caractère personnel par Colabl France (ci-après le "Responsable du Traitement") repose sur les principes de légalité, loyauté et transparence.

<div id='section-id-187'/>

### 7.1. Principe de Légalité

§ Le Responsable du Traitement s'engage à assurer que tout traitement de données à caractère personnel est effectué sur la base d'une base légale valide conformément à l'[article 6 du RGPD](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679#d1e1937-1-1). Les bases légales pouvant justifier le traitement des données personnelles par le Responsable du Traitement incluent, sans s'y limiter, le consentement de la personne concernée, la nécessité du traitement aux fins de l'exécution d'un contrat auquel la personne concernée est partie, le respect d'une obligation légale à laquelle le Responsable du Traitement est soumis, ainsi que la poursuite d'intérêts légitimes poursuivis par le Responsable du Traitement ou par un tiers, sous réserve que les intérêts ou les droits et libertés fondamentaux de la personne concernée ne prévalent pas.

<div id='section-id-191'/>

### 7.2. Principe de Loyauté

§ Le traitement des données à caractère personnel est effectué de manière loyale, ce qui signifie que le Responsable du Traitement doit s'assurer que les personnes concernées sont informées de l'existence du traitement, de ses finalités, de la base juridique du traitement, ainsi que de tout autre renseignement pertinent pour garantir un traitement équitable et transparent.

<div id='section-id-195'/>

### 7.3. Principe de Transparence

§ Le Responsable du Traitement s'engage à fournir aux personnes concernées toutes les informations nécessaires relatives au traitement de leurs données personnelles de manière concise, transparente, intelligible et aisément accessible, en utilisant un langage clair et simple. Ceci inclut, mais ne se limite pas, à l'information sur l'identité du Responsable du Traitement, les finalités du traitement pour lesquelles les données personnelles sont destinées ainsi que la base juridique du traitement, les destinataires ou les catégories de destinataires des données personnelles, le cas échéant, et les droits dont disposent les personnes concernées en vertu du RGPD et des législations nationales applicables.

Le Responsable du Traitement garantit ainsi que toutes les mesures nécessaires sont prises pour assurer que le traitement des données à caractère personnel se déroule dans le respect absolu des principes de légalité, loyauté et transparence, protégeant ainsi les droits et libertés fondamentaux des personnes concernées.

<div id='section-id-201'/>

## 8. Limitation des Finalités

§ Dans le strict respect des obligations imposées par le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi que conformément aux législations nationales en vigueur en matière de protection des données personnelles, Colabl France (ci-après le "Responsable du Traitement") s'engage à observer le principe de limitation des finalités.

<div id='section-id-205'/>

### 8.1. Définition du Principe

§ Le principe de limitation des finalités stipule que les données à caractère personnel doivent être collectées pour des finalités déterminées, explicites et légitimes et ne pas être traitées ultérieurement d'une manière incompatible avec ces finalités initiales. Toute utilisation des données à caractère personnel qui s'écarte de ces finalités prédéfinies est considérée comme non conforme, à moins que le traitement ultérieur ne soit pas interdit par le droit applicable et soit fondé sur une base légale appropriée.

<div id='section-id-209'/>

### 8.2. Application du Principe

- **Finalités prédéfinies** : Avant la collecte des données à caractère personnel, le Responsable du Traitement s'assure de définir clairement les finalités pour lesquelles les données sont collectées. Ces finalités sont communiquées aux personnes concernées au moment de la collecte des données.

- **Compatibilité des finalités** : Le Responsable du Traitement évalue la compatibilité des finalités du traitement ultérieur avec les finalités initiales pour lesquelles les données à caractère personnel ont été collectées. Tout traitement ultérieur à des fins autres que celles pour lesquelles les données ont été collectées requiert une réévaluation de la base légale du traitement et une nouvelle information des personnes concernées.

- **Limitation stricte** : Le Responsable du Traitement s'engage à ne pas traiter les données à caractère personnel d'une manière incompatible avec les finalités initiales. En cas de besoin de traitement pour une nouvelle finalité non compatible, le Responsable du Traitement recueillera le consentement explicite des personnes concernées ou s'appuiera sur une autre base légale spécifiquement prévue par la loi.

<div id='section-id-217'/>

### 8.3. Transparence des Finalités

§ Le Responsable du Traitement garantit la transparence des finalités du traitement envers les personnes concernées, en fournissant, dès la collecte des données et de manière proactive, des informations claires, précises et complètes sur les raisons pour lesquelles leurs données à caractère personnel sont traitées.

En vertu de ce principe, le Responsable du Traitement assure que toutes les actions de traitement des données personnelles respectent scrupuleusement les finalités pour lesquelles ces données ont été collectées, contribuant ainsi à la protection des droits et libertés fondamentaux des personnes concernées et renforçant la confiance dans les pratiques de gestion des données personnelles.

<div id='section-id-223'/>

## 9. Minimisation des données

Conformément aux dispositions impératives du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après dénommé "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi qu'aux législations nationales pertinentes en vigueur en matière de protection des données personnelles, Colabl France (ci-après le "Responsable du Traitement") s'engage à appliquer avec rigueur le principe de minimisation des données.


<div id='section-id-228'/>

### 9.1. Définition du principe

§ Le principe de minimisation des données stipule que les données à caractère personnel doivent être adéquates, pertinentes et limitées à ce qui est nécessaire au regard des finalités pour lesquelles elles sont traitées. Ce principe impose une restriction quant à la quantité de données personnelles collectées, leur étendue, et leur conservation, visant à garantir que seules les données strictement nécessaires pour atteindre les finalités spécifiées sont traitées.


<div id='section-id-233'/>

### 9.2. Mise en Œuvre du principe

- **Évaluation de la pertinence** : Le Responsable du Traitement procède à une évaluation minutieuse des données à caractère personnel qu'il envisage de collecter, afin de s'assurer que chaque donnée recueillie est indispensable à la réalisation de la finalité spécifiée. Cette évaluation est réitérée régulièrement pour s'adapter aux évolutions des finalités du traitement.

- **Limitation de la collecte** : Lors de la collecte des données à caractère personnel, le Responsable du Traitement limite cette collecte aux données strictement nécessaires. Aucune donnée excédentaire ou non essentielle par rapport aux finalités annoncées n'est collectée.

- **Réduction de la conservation** : Le Responsable du Traitement s'engage à ne conserver les données à caractère personnel que pendant la durée strictement nécessaire à l'accomplissement des finalités pour lesquelles elles ont été collectées et traitées, en accord avec les politiques de conservation des données établies et les obligations légales applicables.


<div id='section-id-242'/>

### 9.3. Application pratique

- **Revues périodiques** : Le Responsable du Traitement met en place des revues périodiques des données conservées pour s'assurer que toutes les données à caractère personnel stockées sont toujours nécessaires aux finalités déclarées.
- **Suppression des données inutiles** : Les données jugées non nécessaires ou devenues superflues par rapport aux finalités du traitement sont systématiquement identifiées et supprimées de manière sécurisée.


<div id='section-id-248'/>

### 9.4. Transparence et Responsabilité

§ Le Responsable du Traitement documente les processus et les critères appliqués pour la mise en œuvre du principe de minimisation des données, démontrant ainsi sa conformité avec le RGPD et renforçant la transparence de ses pratiques de traitement vis-à-vis des personnes concernées et des autorités de contrôle.

Le strict respect du principe de minimisation des données témoigne de l'engagement du Responsable du Traitement à garantir et à protéger la vie privée et les droits fondamentaux des personnes concernées, en limitant l'exposition des données personnelles et en minimisant les risques de violation de la protection des données.

<div id='section-id-254'/>

## 10. Exactitude

Dans le cadre du respect inconditionnel des obligations statutaires imposées par le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi que conformément aux législations nationales en vigueur relatives à la protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") s'engage à observer scrupuleusement le principe d'exactitude.

<div id='section-id-258'/>

### 10.1. Définition du principe

§ Le principe d'exactitude prescrit que les données à caractère personnel doivent être exactes et, si nécessaire, tenues à jour; toute inexactitude dans les données à caractère personnel doit être rectifiée sans délai. Ce principe exige du Responsable du Traitement de prendre toutes les mesures raisonnables pour s'assurer que les données personnelles inexactes, au regard des finalités pour lesquelles elles sont traitées, soient effacées ou rectifiées sans tarder.

<div id='section-id-262'/>

### 10.2. Mise en œuvre du principe

- **Vérification de l'exactitude** : Avant le traitement, et aussi régulièrement que nécessaire par la suite, le Responsable du Traitement vérifie l'exactitude des données à caractère personnel collectées. Cette vérification s'appuie sur les informations les plus fiables et les plus récentes disponibles.

- **Actualisation des données** : Le Responsable du Traitement met en place des mécanismes permettant aux personnes concernées de mettre à jour facilement leurs données à caractère personnel. En outre, des procédures internes sont établies pour réviser et actualiser régulièrement les données, notamment en sollicitant la confirmation de leur exactitude auprès des personnes concernées.

- **Correction et suppression** : En cas d'identification de données inexactes, le Responsable du Traitement procède, sans délai indu, à leur correction ou à leur suppression. Les personnes concernées ont le droit de demander la rectification de données inexactes les concernant et, le cas échéant, le droit de compléter des données incomplètes.

<div id='section-id-270'/>

### 10.3. Responsabilités et Engagements

- **Documentation des procédures** : Le Responsable du Traitement documente les procédures mises en place pour garantir l'exactitude des données à caractère personnel, y compris les méthodes de vérification et de correction.

- **Sensibilisation et formation** : Le personnel impliqué dans le traitement des données à caractère personnel est régulièrement formé et sensibilisé aux exigences relatives à l'exactitude des données, afin d'assurer une prise en charge adéquate des corrections et mises à jour.


<div id='section-id-277'/>

### 10.4. Transparence et Information

§ Le Responsable du Traitement informe clairement les personnes concernées sur leur droit à la rectification des données inexactes et sur les modalités pour exercer ce droit, renforçant ainsi la transparence et l'efficacité de la mise en œuvre du principe d'exactitude.

L'adhésion rigoureuse au principe d'exactitude reflète l'engagement du Responsable du Traitement à traiter des données fiables et à jour, essentiel pour respecter l'intégrité des personnes concernées et pour garantir la fiabilité des décisions prises sur la base de ces données.


<div id='section-id-284'/>

## 11. Limitation de la conservation

Conformément aux dispositions prescrites par le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi que suivant les normes établies par les législations nationales pertinentes en matière de protection des données personnelles, Colabl France (ci-après le "Responsable du Traitement") s'engage fermement à respecter le principe de limitation de la conservation.


<div id='section-id-289'/>

### 11.1. Définition du Principe

§ Le principe de limitation de la conservation stipule que les données à caractère personnel doivent être conservées sous une forme permettant l'identification des personnes concernées pour une durée n'excédant pas celle nécessaire au regard des finalités pour lesquelles elles sont traitées. Ce principe impose une gestion prudente de la durée de conservation des données personnelles, garantissant leur suppression ou anonymisation lorsque celles-ci ne sont plus nécessaires aux fins établies.

<div id='section-id-293'/>

### 11.2. Application du Principe

- **Détermination de la durée de conservation** : Le Responsable du Traitement établit et documente, pour chaque catégorie de données à caractère personnel traitées, une durée de conservation précise, basée sur les exigences légales, les finalités du traitement, et les obligations contractuelles. Cette durée est systématiquement réévaluée pour s'assurer de sa pertinence.

- **Révision périodique** : Des procédures de révision périodique sont instaurées pour examiner la nécessité de la conservation des données personnelles. Ces révisions permettent d'identifier et d'agir sur les données dont la période de conservation est expirée.

- **Suppression ou Anonymisation** : À l'expiration de la durée de conservation déterminée, le Responsable du Traitement procède à la suppression sécurisée des données à caractère personnel ou à leur anonymisation, rendant impossible l'identification des personnes concernées.

<div id='section-id-301'/>

### 11.3. Mesures de Mise en Œuvre

- **Politique de conservation** : Le Responsable du Traitement élabore une politique de conservation des données claire, documentant les durées de conservation pour chaque type de donnée et les procédures de suppression ou d'anonymisation.

- **Sensibilisation et Formation** : Les employés impliqués dans le traitement des données sont formés et sensibilisés sur l'importance du respect des durées de conservation et sur les procédures de suppression ou d'anonymisation des données.

<div id='section-id-307'/>

### 11.4. Transparence envers les Personnes Concernées

§ Le Responsable du Traitement informe les personnes concernées, de manière transparente, sur les durées de conservation des données à caractère personnel et sur les droits dont elles disposent relativement à la suppression ou à l'oubli des données les concernant.

L'application rigoureuse du principe de limitation de la conservation est une manifestation de l'engagement du Responsable du Traitement à garantir la protection et le respect de la vie privée des personnes concernées, en ne conservant leurs données personnelles que pour la durée strictement nécessaire aux finalités pour lesquelles elles ont été collectées.

<div id='section-id-313'/>

## 12. Intégrité et confidentialité

En vertu des obligations statutaires émanant du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi que conformément aux prescriptions des législations nationales en vigueur relatives à la protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") s'engage à appliquer avec rigueur les principes d'intégrité et de confidentialité.



<div id='section-id-319'/>

### 12.1. Définition du principe

§ Les principes d'intégrité et de confidentialité exigent que les données à caractère personnel soient traitées de manière à garantir une sécurité adéquate des données, incluant la protection contre le traitement non autorisé ou illicite et contre la perte, la destruction ou les dommages d'origine accidentelle, en utilisant des mesures techniques ou organisationnelles appropriées.

<div id='section-id-323'/>

### 12.2. Mise en œuvre des principes

- **Mesures de sécurité** : Le Responsable du Traitement implémente des mesures de sécurité techniques et organisationnelles avancées pour préserver l'intégrité et la confidentialité des données à caractère personnel. Ces mesures incluent, sans limitation, le cryptage des données, la sécurisation des communications, l'accès restreint aux données personnelles, et la surveillance constante des systèmes de traitement.
- **Gestion des accès** : L'accès aux données à caractère personnel est strictement limité aux individus autorisés, en fonction de leur rôle et de leurs besoins d'accès dans le cadre de leurs fonctions. Des contrôles d'accès rigoureux sont mis en place pour prévenir tout accès non autorisé.
- **Formation et Sensibilisation** : Le Responsable du Traitement assure la formation et la sensibilisation régulières de son personnel concernant les risques liés à la sécurité des données personnelles, les obligations de confidentialité, et les mesures à adopter pour prévenir toute atteinte à l'intégrité et à la confidentialité des données.
- **Gestion des violations de données** : En cas de violation de données à caractère personnel, le Responsable du Traitement dispose de procédures efficaces pour détecter, signaler et investiguer l'incident conformément aux obligations légales. Les personnes concernées et les autorités de contrôle compétentes sont informées dans les délais prescrits par la loi.

<div id='section-id-330'/>

### 12.3. Engagement du Responsable du traitement

- **Audit et révision** : Le Responsable du Traitement s'engage à réaliser périodiquement des audits de sécurité pour évaluer et améliorer la robustesse des mesures de protection des données à caractère personnel.
- **Confidentialité contractuelle** : Les obligations de confidentialité sont clairement définies dans les contrats avec les employés, les sous-traitants et les partenaires commerciaux, assurant une protection cohérente des données à caractère personnel à tous les niveaux de traitement.

<div id='section-id-335'/>

### 12.3. Transparence et Assurance

§ Le Responsable du Traitement garantit la transparence de ses pratiques de sécurisation des données à caractère personnel et s'engage à fournir toutes les assurances nécessaires pour démontrer le respect des principes d'intégrité et de confidentialité, conformément aux exigences du RGPD et des législations nationales.

L'adhésion intransigeante aux principes d'intégrité et de confidentialité souligne l'engagement du Responsable du Traitement à instaurer un environnement de traitement des données à caractère personnel où la sécurité et la protection des droits des personnes concernées sont primordiales et incontestables.

<div id='section-id-341'/>

# Collecte des Données Personnelles

<div id='section-id-343'/>

## 13. Types de données collectées

Conformément aux prescriptions impératives du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après dénommé "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi qu'aux législations nationales applicables en la matière, Colabl France (ci-après le "Responsable du Traitement") s'engage à une collecte de données à caractère personnel respectueuse des principes de minimisation et de transparence. La présente section détaille les catégories de données personnelles collectées par le Responsable du Traitement, distinguant les informations communes à tous les services des données spécifiques selon le service ou l'application.

<div id='section-id-347'/>

### 13.1. Informations communes à tous les services

§ Le Responsable du Traitement collecte les catégories suivantes de données à caractère personnel, jugées nécessaires et pertinentes pour l'inscription, l'utilisation et la gestion des services, logiciels et applications mobiles proposés :

- **Données d'identification** : comprenant, mais sans s'y limiter, le nom, prénom, et, le cas échéant, la copie d'un document d'identité.
- **Coordonnées** : telles que l'adresse électronique, le numéro de téléphone, et l'adresse postale.
- **Données de connexion** : incluant les identifiants et mots de passe permettant l'accès aux services, ainsi que les journaux de connexion et d'utilisation.
- **Données de transaction** : relatives aux opérations financières ou commerciales réalisées via les services, incluant les informations de paiement.
- **Interactions et communications** : données générées par ou collectées à partir des interactions des utilisateurs avec le service, y compris les requêtes, les demandes d'assistance et les retours utilisateurs.

<div id='section-id-357'/>

### 13.2. Données spécifiques selon le service ou l'application

§ En complément des données communes, le Responsable du Traitement peut être amené à collecter des données spécifiques, adaptées aux particularités et exigences fonctionnelles de chaque service ou application :

- **Données de localisation** : pour les services nécessitant une géolocalisation précise pour leur fonctionnement.
Données d'Activité et de Comportement : collectées par le biais de l'utilisation de certaines applications mobiles, incluant les données de santé, de condition physique ou toute autre donnée sensible, sous réserve du consentement explicite de l'utilisateur.
- **Contenus utilisateur** : tels que les photos, vidéos, et autres contenus générés ou partagés par les utilisateurs dans le cadre de l'utilisation du service ou de l'application.
- **Préférences et intérêts** : informations déduites ou directement fournies par les utilisateurs concernant leurs préférences, intérêts ou habitudes de consommation.

§ Le Responsable du Traitement assure que la collecte des données à caractère personnel spécifiques à certains services ou applications est systématiquement précédée d'une information claire des utilisateurs sur les finalités de cette collecte et, le cas échéant, de la collecte de leur consentement explicite, conformément aux exigences du RGPD et des législations nationales pertinentes.

La collecte et le traitement de données à caractère personnel par le Responsable du Traitement sont effectués avec le plus haut degré de diligence et de sécurité, conformément aux principes fondamentaux énoncés par le RGPD, garantissant ainsi le respect et la protection des droits et libertés fondamentaux des personnes concernées.


<div id='section-id-371'/>

## 14. Bases juridiques du traitement (consentement, exécution d'un contrat, obligations légales, intérêts légitimes)

Conformément aux dispositions du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), Colabl France (ci-après le "Responsable du Traitement") s'assure que tout traitement de données à caractère personnel repose sur une base juridique solide et légitime, conformément à l'[article 6 du RGPD](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679#d1e1937-1-1). La présente section détaille les fondements juridiques sur lesquels le Responsable du Traitement s'appuie pour la collecte et le traitement des données personnelles.

<div id='section-id-375'/>

### 14.1. Consentement

§ Le traitement des données à caractère personnel peut être fondé sur le consentement explicite de la personne concernée. Le Responsable du Traitement s'engage à obtenir ce consentement de manière libre, spécifique, éclairée, et univoque pour une ou plusieurs finalités spécifiques. Le consentement est documenté de manière adéquate et peut être retiré à tout moment par la personne concernée, sans porter atteinte à la licéité du traitement fondé sur le consentement effectué avant son retrait.

<div id='section-id-379'/>

### 14.2. Exécution d'un Contrat

§ Le traitement des données à caractère personnel est justifié lorsque cela est nécessaire à l'exécution d'un contrat auquel la personne concernée est partie ou à l'exécution de mesures précontractuelles prises à la demande de celle-ci. Cette base juridique s'applique notamment lors de la fourniture de services, de biens ou de logiciels, requérant la collecte et le traitement de données personnelles pour leur réalisation ou leur fourniture.

<div id='section-id-383'/>

### 14.3. Obligations Légales

§ Le traitement des données à caractère personnel peut être nécessaire pour que le Responsable du Traitement se conforme à une obligation légale à laquelle il est soumis. Cela inclut, sans s'y limiter, les obligations comptables, fiscales, ou toute autre exigence réglementaire nécessitant le traitement des données personnelles.

<div id='section-id-387'/>

### 14.4. Intérêts Légitimes

§ Le Responsable du Traitement peut également procéder au traitement des données à caractère personnel sur la base de ses intérêts légitimes ou ceux d'un tiers, à condition que ces intérêts ne soient pas outrepassés par les intérêts ou les droits et libertés fondamentaux de la personne concernée. Les intérêts légitimes incluent, mais ne sont pas limités à, les besoins en matière de sécurité informatique, de prévention de la fraude, de gestion interne, ou d'amélioration continue des services et produits proposés.

§ Le Responsable du Traitement s'engage à effectuer une évaluation minutieuse lorsque le traitement est fondé sur des intérêts légitimes, garantissant un équilibre entre ces intérêts et les droits et libertés des personnes concernées. Cette évaluation est documentée de manière adéquate et mise à disposition des personnes concernées sur demande.

Le Responsable du Traitement assure que la collecte et le traitement de données à caractère personnel, quelle que soit la base juridique invoquée, sont effectués dans le plein respect des dispositions du RGPD et des législations nationales applicables, garantissant ainsi la protection et la sécurité des données personnelles des personnes concernées.


<div id='section-id-396'/>

## 15. Modalités de collecte (directe/indirecte)

Conformément aux obligations légales édictées par le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi qu'aux législations nationales en vigueur en matière de protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") met en œuvre des modalités de collecte des données à caractère personnel, distinguant la collecte directe auprès des personnes concernées de la collecte indirecte à partir de sources tierces.

<div id='section-id-400'/>

### 15.1. Collecte Directe

§ La collecte directe des données à caractère personnel se fait auprès des personnes concernées lorsqu'elles fournissent volontairement leurs données dans le cadre de l'utilisation des services, logiciels, et applications mobiles proposés par le Responsable du Traitement. Cette collecte peut intervenir dans divers contextes, notamment :

- **Lors de la création de comptes utilisateurs** : enregistrement de données d'identification et de contact nécessaires à la gestion de l'accès aux services.
- **Au cours des transactions commerciales** : saisie des informations de paiement et autres données de transaction lors de l'achat de biens ou services.
- **Dans le cadre de demandes de support ou de contact** : collecte d'informations fournies par les utilisateurs lorsqu'ils soumettent des demandes d'assistance ou des requêtes via les canaux de communication établis.

§ Le Responsable du Traitement s'assure que les personnes concernées sont pleinement informées des finalités de la collecte de leurs données à caractère personnel, conformément aux exigences de transparence établies par le RGPD.

<div id='section-id-410'/>

### 15.2. Collecte Indirecte

§ La collecte indirecte de données à caractère personnel peut être effectuée par le Responsable du Traitement à partir de sources accessibles au public ou obtenues de tiers, dans les cas où la législation applicable autorise une telle collecte. Cette modalité de collecte est employée de manière limitée et est strictement encadrée, à savoir :

- **Sources publiques** : recueil de données à partir de registres publics, publications ou tout autre source d'information publiquement accessible.
- **Partenaires Commerciaux ou Tiers** : acquisition de données personnelles à partir de partenaires fiables, dans le respect des exigences légales, et avec la garantie que les personnes concernées ont été informées de la transmission de leurs données au Responsable du Traitement.

§ Dans chaque cas de collecte indirecte, le Responsable du Traitement vérifie la légitimité de la source des données et s'assure que les personnes concernées ont été dûment informées de la collecte de leurs données ou disposent d'un moyen facile et accessible pour exercer leurs droits, y compris le droit d'opposition au traitement de leurs données.

Le Responsable du Traitement s'engage à traiter toutes les données à caractère personnel, qu'elles soient collectées directement auprès des personnes concernées ou de manière indirecte, avec le plus haut niveau de diligence et de sécurité, en conformité avec les principes établis par le RGPD et les législations nationales pertinentes.

<div id='section-id-421'/>

# Utilisation des Données

<div id='section-id-423'/>

## 16. Finalités générales du traitement

Dans le strict respect des dispositions légales édictées par le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi que conformément aux législations nationales applicables en matière de protection des données personnelles, Colabl France (ci-après le "Responsable du Traitement") s'engage à utiliser les données à caractère personnel collectées auprès des personnes concernées exclusivement pour des finalités légitimes, spécifiquement définies et clairement communiquées aux personnes concernées. Les finalités générales pour lesquelles les données à caractère personnel peuvent être traitées incluent, sans s'y limiter, les aspects suivants :


<div id='section-id-428'/>

### 16.1. Fourniture et gestion des services

§ Le traitement des données à caractère personnel est essentiel pour la fourniture, la gestion, et l'amélioration des services, logiciels, et applications mobiles proposés par le Responsable du Traitement. Cela inclut, mais n'est pas limité à, l'enregistrement des utilisateurs, la personnalisation des services selon les préférences des utilisateurs, la fourniture de support client, et la gestion des comptes utilisateurs.

<div id='section-id-432'/>

### 16.2. Transactions commerciales

§ Les données à caractère personnel sont utilisées pour faciliter les transactions commerciales, incluant la gestion des paiements, la facturation, la livraison de biens et services, ainsi que pour le traitement des retours et des réclamations.

<div id='section-id-436'/>

### 16.3. Communication avec les utilisateurs

§ Le Responsable du Traitement peut utiliser les données à caractère personnel pour communiquer avec les utilisateurs, notamment pour l'envoi d'informations relatives aux services, des notifications importantes, des mises à jour de services, et des offres promotionnelles, sous réserve du consentement explicite des personnes concernées lorsque cela est requis par la loi.

<div id='section-id-440'/>

### 16.4. Amélioration des services et développement de nouveaux produits

§ Les données collectées servent également à analyser et comprendre comment les services sont utilisés, ce qui permet au Responsable du Traitement d'améliorer l'expérience utilisateur, de développer de nouveaux produits et services, et d'effectuer des recherches et des analyses de marché.

<div id='section-id-444'/>

### 16.5. Sécurité et conformité légale

§ Le traitement des données à caractère personnel est crucial pour assurer la sécurité des services et des utilisateurs, prévenir et détecter les fraudes, les abus, ou toute autre activité malveillante. De plus, certaines données peuvent être traitées pour se conformer aux obligations légales et réglementaires incombant au Responsable du Traitement, incluant les exigences comptables et fiscales.

Le Responsable du Traitement garantit que tout traitement de données à caractère personnel est effectué en conformité avec les principes fondamentaux de protection des données établis par le RGPD, y compris la limitation des finalités, minimisant ainsi l'utilisation des données à ce qui est strictement nécessaire pour atteindre les finalités légitimes pour lesquelles elles ont été collectées.

<div id='section-id-450'/>

## 17. Finalités

En vertu des obligations statutaires découlant du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi qu'en conformité avec les législations nationales applicables en matière de protection des données personnelles, Colabl France (ci-après le "Responsable du Traitement") reconnaît que certaines catégories de services, logiciels et applications mobiles qu'il propose peuvent nécessiter le traitement des données à caractère personnel pour des finalités spécifiques, outrepassant les finalités générales de traitement susmentionnées. Ces finalités spécifiques, inhérentes à la nature particulière de certains services ou applications, sont définies comme suit :

<div id='section-id-454'/>

### 17.1. Services de Localisation

§ Pour les services nécessitant des fonctionnalités de géolocalisation, le traitement des données de localisation des utilisateurs est effectué afin de fournir des services basés sur la localisation géographique, tels que la navigation, la recherche de points d'intérêt ou la fourniture de contenus personnalisés en fonction de la localisation de l'utilisateur.

<div id='section-id-458'/>

### 17.2. Services de santé et de bien-être

§ Lorsque le Responsable du Traitement offre des services liés à la santé, au bien-être ou à l'activité physique, le traitement des données à caractère personnel peut inclure des données sensibles relatives à la santé physique ou mentale de l'utilisateur. Ce traitement est effectué dans le but de fournir des conseils personnalisés, des suivis d'activité ou des services de coaching, sous réserve d'un consentement explicite et informé de la part de l'utilisateur conformément aux exigences du RGPD.

<div id='section-id-462'/>

### 17.3. Services d'analyse comportementale

§ Pour les services impliquant l'analyse du comportement utilisateur en ligne, les données collectées peuvent être utilisées pour comprendre les préférences et les habitudes des utilisateurs, dans le but d'améliorer l'expérience utilisateur, de personnaliser les contenus et les publicités, et de développer de nouvelles fonctionnalités répondant aux attentes des utilisateurs.

<div id='section-id-466'/>

### 17.4. Services de support et de Communication interactifs

§ Lorsque le service implique un support client interactif, tel que les chatbots ou les services d'assistance en ligne, le traitement des données à caractère personnel peut inclure des échanges de communications entre l'utilisateur et le service, dans le but de fournir une assistance personnalisée et efficace aux utilisateurs.

<div id='section-id-470'/>

### 17.5. Participation à des Événements ou à des programmes de fidélité

§ Pour les services offrant la possibilité de participer à des événements, des concours ou des programmes de fidélité, le traitement des données à caractère personnel peut être requis pour gérer la participation de l'utilisateur, attribuer des récompenses, et communiquer des informations relatives à ces événements ou programmes.

Le Responsable du Traitement s'engage à ce que, pour chacune de ces finalités spécifiques, les principes de minimisation des données, de limitation de la finalité, et de protection renforcée des données sensibles soient strictement observés, assurant ainsi que le traitement des données à caractère personnel est effectué dans le plus strict respect des droits et libertés des personnes concernées, conformément au cadre légal établi par le RGPD et les législations nationales pertinentes.

<div id='section-id-476'/>

## 18. Profilage et décision automatique, le cas échéant

Conformément aux exigences prescrites par le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi qu'aux législations nationales en vigueur en matière de protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") s'engage à une mise en œuvre prudente et transparente des activités de profilage et des décisions automatisées, lorsque celles-ci sont employées dans le cadre de ses services, logiciels et applications mobiles.

<div id='section-id-480'/>

### 18.1. Définition et Application

§ Le profilage est défini comme toute forme de traitement automatisé de données à caractère personnel consistant à utiliser ces données pour évaluer certains aspects personnels relatifs à une personne physique, en particulier pour analyser ou prédire des éléments concernant les performances professionnelles, la situation économique, la santé, les préférences personnelles, les intérêts, la fiabilité, le comportement, la localisation ou les déplacements de cette personne physique.

§ Le Responsable du Traitement peut recourir au profilage et à la prise de décision automatisée dans le but d'améliorer l'expérience utilisateur, de personnaliser les services offerts et de cibler de manière plus efficace les offres et les communications marketing, sous réserve des conditions suivantes :

<div id='section-id-486'/>

#### 18.1.1 Consentement et Droits des personnes concernées

- **Consentement explicite** : Le consentement explicite de la personne concernée est obtenu avant toute opération de profilage lorsque celui-ci produit des effets juridiques concernant la personne ou l'affecte de manière significative.
- **Information transparente** : Les personnes concernées sont informées de manière claire et détaillée sur l'existence de profilage, les finalités poursuivies, et les conséquences attendues de telles activités.
- **Droit d'opposition** : Le Responsable du Traitement garantit aux personnes concernées le droit de s'opposer à tout moment au profilage, conformément à l'[article 21 du RGPD](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679#d1e2849-1-1).


<div id='section-id-493'/>

### 18.1.2. Garanties et Mesures de sécurité

- **Évaluation d'impact** : Une évaluation d'impact relative à la protection des données est réalisée avant la mise en œuvre de tout traitement impliquant profilage ou décisions automatisées, afin d'identifier et de minimiser les risques pour les droits et libertés des personnes concernées.
- **Intervention humaine** : Le Responsable du Traitement assure qu'en cas de décision fondée exclusivement sur un traitement automatisé, y compris le profilage, des mécanismes sont mis en place pour permettre l'intervention humaine, pour exprimer le point de vue de la personne concernée et pour contester la décision.
- **Précision des données** : Des mesures adéquates sont prises pour garantir que les données utilisées pour le profilage sont exactes et, si nécessaire, mises à jour; toute inexactitude est corrigée sans délai.

Le Responsable du Traitement s'engage à respecter scrupuleusement les principes de protection des données et les droits fondamentaux des personnes concernées lors de l'utilisation de profilage et de décisions automatisées, conformément aux dispositions légales et réglementaires en vigueur, assurant ainsi une approche équilibrée et équitable de l'utilisation des technologies avancées dans le traitement des données à caractère personnel.

<div id='section-id-501'/>

# Partage et Transfert des données

<div id='section-id-503'/>

## 19. Transferts internes (entre différents services de l'entité)

Conformément aux obligations découlant du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi qu'aux législations nationales en vigueur relatives à la protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") s'engage à réguler les transferts internes de données à caractère personnel entre différents services de l'entité dans le respect le plus strict des principes de protection des données énoncés par le RGPD.

<div id='section-id-507'/>

### 19.1. Cadre juridique et organisationnel

§ Le Responsable du Traitement assure que tout transfert interne de données à caractère personnel entre les différents services de l'entité est effectué conformément à un cadre juridique et organisationnel défini, visant à garantir la sécurité et la confidentialité des données transférées. Ce cadre comprend :

- **Politiques internes de transfert** : Des politiques et procédures internes spécifiques régissent les transferts de données à caractère personnel entre les services, en stipulant clairement les conditions et les modalités de ces transferts, ainsi que les obligations des parties impliquées.
- **Bases légales** : Les transferts internes de données personnelles s'appuient sur des bases légales solides, conformément aux exigences du RGPD, incluant mais ne se limitant pas à, le consentement des personnes concernées, la nécessité pour l'exécution d'un contrat, les obligations légales, ou les intérêts légitimes du Responsable du Traitement.

<div id='section-id-514'/>

### 19.2. Mesures de Sécurité et de confidentialité

- **Accords de confidentialité** : Les employés et les services impliqués dans le transfert des données à caractère personnel sont soumis à des accords de confidentialité et des engagements de non-divulgation, garantissant ainsi que les données sont traitées avec le plus haut degré de confidentialité.
- **Sécurisation des transferts** : Des mesures techniques et organisationnelles appropriées sont mises en place pour sécuriser les transferts de données à caractère personnel entre les services, incluant le chiffrement des données lors des transferts et l'accès sécurisé et restreint aux données transférées.
- **Formation et sensibilisation** : Le personnel impliqué dans les transferts de données est régulièrement formé et sensibilisé aux obligations légales et aux meilleures pratiques en matière de protection des données personnelles, afin d'assurer une manipulation adéquate des données lors des transferts internes.

<div id='section-id-520'/>

### 19.3. Responsabilité et conformité

§ Le Responsable du Traitement conserve une trace documentée des transferts internes de données à caractère personnel, y compris la nature des données transférées, les services impliqués, les finalités du transfert, et les mesures de sécurité appliquées, afin de démontrer la conformité avec les obligations légales en matière de protection des données.

Le Responsable du Traitement s'engage à effectuer un examen périodique des pratiques de transfert interne des données à caractère personnel, en vue de leur amélioration continue et de l'adaptation aux évolutions législatives et technologiques, garantissant ainsi un niveau de protection optimal des données à caractère personnel au sein de l'entité.

<div id='section-id-526'/>

## 20. Divulgation à des tiers (sous-traitants, partenaires commerciaux)

<div id='section-id-528'/>

### 20.1. Responsabilité

§ En vertu des obligations imposées par le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi que conformément aux législations nationales en vigueur relatives à la protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") s'engage à encadrer strictement toute divulgation de données à caractère personnel à des tiers, incluant les sous-traitants et les partenaires commerciaux, dans le cadre de ses activités.

<div id='section-id-532'/>

### 20.2. Cadre contractuel et obligations des tiers

- **Accords de traitement de données** : Avant toute divulgation de données à caractère personnel à des tiers, le Responsable du Traitement s'assure de la mise en place d'accords de traitement de données conformes au RGPD, précisant les obligations des tiers en matière de protection des données, les finalités du traitement, ainsi que les mesures de sécurité à appliquer.
- **Vérification de la conformité** : Le Responsable du Traitement procède à une évaluation rigoureuse de la conformité des pratiques de protection des données des tiers avec le RGPD et les législations nationales applicables, garantissant ainsi que les données personnelles sont traitées avec un niveau de protection adéquat.
- **Responsabilité des sous-traitants** : Les sous-traitants sont tenus de traiter les données personnelles exclusivement sur instructions du Responsable du Traitement et sont soumis à des obligations strictes en matière de protection des données, incluant l'obligation de mettre en œuvre des mesures techniques et organisationnelles appropriées pour garantir la sécurité des données.

<div id='section-id-538'/>

### 20.3. Transparence et Consentement des personnes concernées

- **Information des personnes concernées** : Le Responsable du Traitement s'engage à informer les personnes concernées de la divulgation de leurs données à caractère personnel à des tiers, en précisant l'identité des destinataires, les finalités du partage des données et, le cas échéant, les pays dans lesquels les données peuvent être transférées.
- **Consentement** : Lorsque requis par le RGPD ou les législations nationales, le consentement explicite des personnes concernées est obtenu avant la divulgation de leurs données à caractère personnel à des tiers.

<div id='section-id-543'/>

### 20.4. Transferts Internationaux

§ Dans le cas de transferts internationaux de données à caractère personnel à des tiers situés en dehors de l'Espace Économique Européen, le Responsable du Traitement s'assure que ces transferts sont effectués conformément aux dispositions du RGPD, en utilisant des mécanismes de transfert adéquats tels que les clauses contractuelles types approuvées par la Commission européenne, ou en vérifiant que le destinataire assure un niveau de protection adéquat des données personnelles.

Le Responsable du Traitement maintient une vigilance constante sur la gestion des données à caractère personnel divulguées à des tiers, en s'assurant que ces derniers respectent leurs obligations en matière de protection des données et en prenant toutes les mesures nécessaires pour préserver la confidentialité, l'intégrité et la sécurité des données personnelles, conformément aux exigences légales et réglementaires en vigueur.

<div id='section-id-549'/>

## 21. Transferts internationaux de données (garanties et mécanismes de protection)

Conformément aux exigences édictées par le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi qu'aux dispositions pertinentes des législations nationales en vigueur en matière de protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") s'engage à observer des garanties strictes et à mettre en œuvre des mécanismes de protection robustes en matière de transferts internationaux de données à caractère personnel.

<div id='section-id-553'/>

### 21.1. Principes généraux

§ Le Responsable du Traitement garantit que tout transfert de données à caractère personnel en dehors de l'Espace Économique Européen (EEE) est effectué dans le plein respect des exigences du RGPD et uniquement vers des pays reconnus par la Commission européenne comme offrant un niveau de protection adéquat, ou en présence de garanties appropriées assurant un niveau de protection équivalent des données personnelles.

<div id='section-id-557'/>

### 21.2. Garanties et Mécanismes de protection

- **Clauses Contractuelles Types (CCT)** : Le Responsable du Traitement utilise des clauses contractuelles types approuvées par la Commission européenne pour les transferts de données à caractère personnel vers des pays n'ayant pas fait l'objet d'une décision d'adéquation. Ces clauses contractuelles imposent des obligations strictes en matière de protection des données au destinataire des données dans le pays tiers.
- **Règles d'entreprise contraignantes (Binding Corporate Rules, BCR)** : Pour les transferts de données au sein d'un même groupe d'entreprises, le Responsable du Traitement peut mettre en place des BCR approuvées par les autorités de contrôle compétentes, offrant des garanties solides en matière de protection des données personnelles et de droits des personnes concernées.
- **Garanties supplémentaires** : Dans les cas où les mécanismes susmentionnés ne peuvent être appliqués, le Responsable du Traitement s'engage à recourir à des garanties supplémentaires conformes au RGPD, telles que des codes de conduite approuvés, des mécanismes de certification, ou des clauses contractuelles autorisées par une autorité de contrôle compétente.

<div id='section-id-563'/>

### 21.3. Droits des Personnes Concernées et Recours

§ Le Responsable du Traitement assure que les personnes concernées sont informées des transferts internationaux de leurs données à caractère personnel, des garanties mises en place pour protéger leurs données, et de leurs droits à obtenir une copie de ces garanties. Les personnes concernées disposent de droits spécifiques et de recours en cas de violation de leurs données dans le cadre de transferts internationaux, conformément au RGPD.

<div id='section-id-567'/>

### 21.4. Évaluation et Réévaluation des Transferts

§ Le Responsable du Traitement procède à une évaluation rigoureuse des conditions de protection des données dans le pays de destination avant tout transfert international et réévalue périodiquement cette protection en tenant compte de l'évolution de la législation et des pratiques en matière de protection des données dans ledit pays.

Le Responsable du Traitement s'engage à maintenir un niveau de protection des données à caractère personnel transférées à l'international équivalent à celui offert au sein de l'EEE, garantissant ainsi la sécurité, la confidentialité, et l'intégrité des données personnelles conformément aux standards les plus élevés imposés par le RGPD et les législations nationales.

<div id='section-id-573'/>

# Droits des Utilisateurs

<div id='section-id-575'/>

## 22. Détail des droits accordés par le RGPD (accès, rectification, suppression, limitation, portabilité, opposition)

En conformité avec les dispositions du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), Colabl France (ci-après désigné le "Responsable du Traitement") reconnaît et garantit l'exercice des droits suivants accordés aux personnes concernées :

<div id='section-id-579'/>

### 22.1. Droit d'Accès

§ Les personnes concernées ont le droit d'obtenir du Responsable du Traitement la confirmation que des données à caractère personnel les concernant sont ou ne sont pas traitées et, lorsqu'elles le sont, l'accès auxdites données ainsi qu'à des informations détaillées concernant les finalités du traitement, les catégories de données traitées, les destinataires ou catégories de destinataires, la durée de conservation envisagée, et les droits de rectification, d'effacement, de limitation du traitement et d'opposition.

<div id='section-id-583'/>

### 22.2. Droit de Rectification

§ Le droit de rectification permet aux personnes concernées d'exiger que le Responsable du Traitement corrige sans délai les données à caractère personnel inexactes les concernant. Elles ont également le droit de faire compléter les données personnelles incomplètes, y compris par le biais d'une déclaration complémentaire.

<div id='section-id-587'/>

### 22.3. Droit à l'Effacement (« Droit à l'Oubli »)

§ Les personnes concernées ont le droit d'obtenir du Responsable du Traitement l'effacement, dans les meilleurs délais, de données à caractère personnel les concernant lorsque l'un des motifs prévus par le RGPD s'applique, tels que le retrait du consentement, l'opposition au traitement, ou l'inutilité des données pour les finalités du traitement.

<div id='section-id-591'/>

### 22.4. Droit à la Limitation du Traitement

§ Ce droit permet aux personnes concernées de demander la limitation du traitement de leurs données personnelles dans certaines circonstances, notamment en cas de contestation de l'exactitude des données, ou lorsque le traitement est illicite et que la personne concernée s'oppose à leur effacement et demande à la place la limitation de leur utilisation.

<div id='section-id-595'/>

### 22.5. Droit à la Portabilité des Données

§ Les personnes concernées ont le droit de recevoir les données à caractère personnel les concernant qu'elles ont fournies au Responsable du Traitement, dans un format structuré, couramment utilisé et lisible par machine, et ont le droit de transmettre ces données à un autre responsable du traitement sans que le Responsable du Traitement à qui les données à caractère personnel ont été fournies y fasse obstacle.

<div id='section-id-599'/>

### 22.6. Droit d'Opposition

§ Les personnes concernées ont le droit de s'opposer à tout moment, pour des raisons tenant à leur situation particulière, au traitement des données à caractère personnel les concernant basées sur les intérêts légitimes du Responsable du Traitement, y compris le profilage. Le Responsable du Traitement ne pourra plus traiter les données à moins de démontrer qu'il existe des motifs légitimes et impérieux pour le traitement qui prévalent sur les intérêts, droits et libertés de la personne concernée, ou pour la constatation, l'exercice ou la défense de droits en justice.

Le Responsable du Traitement s'engage à fournir aux personnes concernées tous les moyens nécessaires pour exercer efficacement leurs droits conformément au RGPD, en mettant en place des procédures claires et accessibles et en répondant sans délai aux demandes formulées dans le cadre de l'exercice de ces droits.

<div id='section-id-605'/>

## 23. Modalités d'exercice de ces droits

En conformité avec le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi qu'aux dispositions des législations nationales en vigueur en matière de protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") établit les modalités suivantes permettant aux personnes concernées d'exercer efficacement leurs droits :

<div id='section-id-609'/>

### 23.1. Procédures de demande

- **Point de contact** : Le Responsable du Traitement désigne un point de contact spécifique (par exemple, un Délégué à la Protection des Données) pour recevoir et gérer les demandes relatives aux droits des personnes concernées. Les coordonnées de ce point de contact sont clairement communiquées aux personnes concernées.
- **Forme de la demande** : Les demandes d'exercice des droits peuvent être soumises par les moyens de communication prévus par le Responsable du Traitement, y compris mais non limité à, courrier électronique, plateforme en ligne, ou courrier postal. Le Responsable du Traitement peut fournir des formulaires spécifiques pour faciliter l'exercice des droits, sans que cela ne soit une obligation pour la personne concernée.
- **Identification de la personne concernée** : Afin de traiter la demande, le Responsable du Traitement peut demander à la personne concernée de fournir des informations supplémentaires nécessaires à son identification et à la confirmation de son identité, garantissant ainsi que les données ne sont pas divulguées à des tiers non autorisés.

<div id='section-id-615'/>

### 23.2. Délais de traitement

§ Le Responsable du Traitement s'engage à répondre aux demandes d'exercice des droits des personnes concernées dans les meilleurs délais et, en tout état de cause, dans un délai d'un mois à compter de la réception de la demande. Ce délai peut être prolongé de deux mois supplémentaires compte tenu de la complexité et du nombre de demandes, le Responsable du Traitement informant la personne concernée de cette prolongation et des motifs du retard dans le délai initial d'un mois.

<div id='section-id-619'/>

### 23.3. Coût de la demande

§ L'exercice des droits prévus par le RGPD est, en principe, gratuit pour la personne concernée. Toutefois, le Responsable du Traitement peut imposer des frais raisonnables basés sur les coûts administratifs pour les demandes manifestement infondées ou excessives, notamment en raison de leur caractère répétitif. Le Responsable du Traitement informe la personne concernée de ces frais avant de traiter la demande.

<div id='section-id-623'/>

### 23.4. Refus de satisfaction aux demandes

§ Dans les cas où le Responsable du Traitement ne prend pas de mesures à la demande de la personne concernée, il informe sans délai, et au plus tard dans un délai d'un mois à compter de la réception de la demande, la personne concernée des motifs de son inaction et de la possibilité d'introduire une réclamation auprès d'une autorité de contrôle et de former un recours judiciaire.

Le Responsable du Traitement s'assure que toutes les procédures mises en place pour l'exercice des droits des personnes concernées sont accessibles, simplifiées, et efficaces, garantissant ainsi le plein respect des droits conférés par le RGPD et les législations nationales en matière de protection des données personnelles.

<div id='section-id-629'/>

## 24. Contact du délégué à la protection des données (DPO)

Conformément aux obligations imposées par le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi qu'aux dispositions législatives nationales applicables en matière de protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") a procédé à la nomination d'un Délégué à la Protection des Données (DPO).

Le DPO joue un rôle crucial dans la supervision de la conformité aux dispositions du RGPD et des législations nationales relatives à la protection des données, en veillant à ce que les pratiques de traitement des données à caractère personnel respectent les droits fondamentaux et libertés des personnes concernées.

<div id='section-id-635'/>

### 24.1. Coordonnées du DPO

§ Le DPO est joignable pour toute question relative à la protection des données à caractère personnel, pour toute demande d'exercice des droits conférés par le RGPD aux personnes concernées, ainsi que pour toute consultation concernant les pratiques de traitement des données au sein de l'entité. Les coordonnées du DPO sont les suivantes :

> **Nom et Prénom du représentant :** [DIRECTEUR_PUBLICATION_COLABL] \
> **Adresse Postale :** [ADRESSE_POSTALE_SIEGE_COLABL] \
> **Numéro de téléphone :** [NUMERO_TEL_STANDARD_COLABL] \
> **Adresse électronique :** [EMAIL_RGPD_COLABL]

§ Le DPO est disponible pour fournir l'assistance nécessaire, pour réceptionner les réclamations, pour conseiller et pour informer tant le Responsable du Traitement que les personnes concernées sur leurs obligations et droits en matière de protection des données personnelles.


<div id='section-id-647'/>

### 24.2. Rôle et Responsabilités du DPO

§ Le DPO assure une variété de fonctions essentielles, incluant mais ne se limitant pas à :

- **Surveillance de la conformité** : Contrôler le respect du RGPD, des législations nationales relatives à la protection des données, ainsi que des politiques internes concernant la protection des données personnelles.
- **Conseil** : Fournir des conseils au Responsable du Traitement et aux employés traitant des données personnelles sur leurs obligations légales.
- **Formation** : Organiser des sessions de formation et de sensibilisation à la protection des données pour le personnel.
- **Coopération** avec les Autorités de Contrôle : Servir de point de contact avec les autorités de protection des données et coopérer avec ces entités.

Le Responsable du Traitement s'engage à garantir que le DPO est impliqué, de manière appropriée, dans toutes les questions relatives à la protection des données personnelles et bénéficie des ressources nécessaires pour mener à bien ses missions conformément au RGPD et aux législations nationales.

<div id='section-id-658'/>

# Sécurité des Données

<div id='section-id-660'/>

## 25. Mesures techniques et organisationnelles mises en place

En stricte conformité avec les exigences du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi que conformément aux dispositions législatives nationales applicables en matière de protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") s'engage à mettre en œuvre des mesures techniques et organisationnelles rigoureuses afin de garantir un niveau de sécurité adapté au risque associé au traitement des données à caractère personnel.

<div id='section-id-664'/>

### 25.1. Mesures techniques

- **Cryptage des données** : Utilisation de technologies de cryptage avancées pour assurer la confidentialité des données à caractère personnel lors de leur stockage et de leur transfert.
- **Sécurité des réseaux** : Mise en place de solutions de sécurité des réseaux, incluant des pare-feu, des systèmes de détection et de prévention des intrusions, afin de protéger contre les accès non autorisés, les attaques informatiques et les autres menaces de sécurité.
- **Accès sécurisé** : Déploiement de mécanismes d'authentification forte et de contrôle d'accès basé sur les rôles pour restreindre l'accès aux données à caractère personnel uniquement au personnel autorisé en fonction de leurs fonctions spécifiques.
- **Sauvegarde et récupération** : Mise en œuvre de systèmes de sauvegarde régulière et de solutions de récupération des données pour prévenir la perte de données et garantir la continuité des activités en cas d'incident.


<div id='section-id-672'/>

### 25.2. Mesures organisationnelles

- **Politiques de sécurité des données** : Élaboration et mise en œuvre de politiques de sécurité des données détaillées, comprenant des directives spécifiques sur le traitement sécurisé des données à caractère personnel.
- **Formation et sensibilisation** : Organisation de sessions de formation régulières pour le personnel impliqué dans le traitement des données à caractère personnel, visant à sensibiliser sur les risques liés à la sécurité des données et sur les pratiques à adopter pour les atténuer.
- **Gestion des incidents de sécurité** : Établissement de procédures claires pour la détection, le signalement et la gestion des incidents de sécurité affectant les données à caractère personnel, incluant la notification aux autorités de contrôle et, le cas échéant, aux personnes concernées, conformément aux exigences du RGPD.
- **Évaluation des risques et tests de sécurité** : Réalisation d'évaluations régulières des risques liés au traitement des données à caractère personnel et mise en place de tests de sécurité périodiques (par exemple, tests de pénétration, audits de sécurité) pour identifier et corriger les vulnérabilités.

Le Responsable du Traitement garantit que les mesures techniques et organisationnelles mises en place sont régulièrement révisées et mises à jour pour refléter l'évolution des menaces de sécurité, des technologies de protection des données et des meilleures pratiques du secteur, assurant ainsi la protection la plus robuste possible des données à caractère personnel contre tout traitement non autorisé ou illicite, contre la perte, la destruction ou les dommages d'origine accidentelle.

<div id='section-id-681'/>

## 26. Gestion des violations de données

En accord avec les obligations stipulées par le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi que conformément aux législations nationales en vigueur relatives à la protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") instaure une procédure rigoureuse pour la gestion et la notification des violations de données à caractère personnel.

<div id='section-id-685'/>

### 26.1. Détection et Évaluation des violations

- **Procédures de détection** : Le Responsable du Traitement met en place des systèmes et des processus permettant la détection rapide de toute violation potentielle des données à caractère personnel, incluant les accès non autorisés, les pertes, les altérations ou les divulgations.
- **Évaluation des risques** : À la suite de la détection d'une violation de données, une évaluation immédiate est effectuée pour déterminer l'ampleur de la violation et les risques potentiels pour les droits et libertés des personnes concernées.

<div id='section-id-690'/>

### 26.2. Notification des Violations

- **Autorités de contrôle** : Conformément à l'[article 33 du RGPD](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679#d1e3479-1-1), le Responsable du Traitement notifie la violation de données à caractère personnel à l'autorité de contrôle compétente sans retard injustifié et, si possible, dans les 72 heures après en avoir pris connaissance, à moins que la violation ne présente pas de risque pour les droits et libertés des personnes concernées.
- **Personnes Concernées** : Conformément à l'[article 34 du RGPD](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679#d1e3535-1-1), lorsque la violation de données est susceptible d'engendrer un risque élevé pour les droits et libertés des personnes concernées, le Responsable du Traitement communique la violation à ces dernières sans retard injustifié, en fournissant une description claire de la nature de la violation, les recommandations pour atténuer les potentiels effets négatifs, ainsi que les mesures proposées ou prises pour y remédier.

<div id='section-id-695'/>

### 26.3. Mesures correctives et préventives

- **Analyse et Contention** : Le Responsable du Traitement procède à une analyse approfondie pour identifier les causes de la violation et mettre en œuvre des mesures immédiates pour contenir et limiter les effets de la violation.
- **Actions correctives** : Des mesures correctives sont prises pour remédier aux vulnérabilités identifiées dans les processus ou les systèmes de sécurité, afin de prévenir la récurrence de violations similaires.
- **Amélioration continue** : Le Responsable du Traitement intègre les enseignements tirés de chaque violation dans sa stratégie globale de sécurité des données, en vue d'améliorer continuellement les mesures de protection des données à caractère personnel.

<div id='section-id-701'/>

### 26.4. Documentation et Revue

§ Toutes les violations de données et les mesures prises en réponse sont documentées de manière exhaustive par le Responsable du Traitement. Cette documentation sert de base pour les revues périodiques des politiques et des procédures de sécurité des données, en vue de leur amélioration continue conformément aux meilleures pratiques et aux exigences légales.

Le Responsable du Traitement s'engage à assurer une gestion transparente et efficace des violations de données à caractère personnel, en vue de protéger les intérêts des personnes concernées et de maintenir la confiance dans ses pratiques de traitement des données.

<div id='section-id-707'/>

## 27. Cookies et Technologies Similaires

Dans le cadre de son engagement à respecter la vie privée des utilisateurs et en conformité avec les obligations légales découlant du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi que les directives spécifiques relatives à l'utilisation des cookies et technologies similaires émises par les autorités de protection des données compétentes, Colabl France (ci-après désigné le "Responsable du Traitement") souhaite informer les utilisateurs de son site Internet, de ses applications mobiles et de ses services en ligne sur l'usage des cookies et technologies similaires.

<div id='section-id-711'/>

### 27.1. Utilisation de cookies et technologies similaires

§ Le Responsable du Traitement utilise des cookies et des technologies similaires pour améliorer l'expérience des utilisateurs sur ses plateformes numériques, pour faciliter la navigation sur le site, pour sécuriser les transactions en ligne et pour personnaliser les contenus proposés aux utilisateurs. Ces outils jouent un rôle essentiel dans le fonctionnement efficace des services numériques et contribuent à l'optimisation continue des interfaces et des fonctionnalités proposées.

<div id='section-id-715'/>

### 27.2. Finalités du suivi

§ Les cookies et technologies similaires sont employés à diverses fins, incluant, sans s'y limiter, la gestion des sessions utilisateurs, la mémorisation des préférences de navigation, l'analyse de l'utilisation des services pour en améliorer la qualité, le ciblage publicitaire basé sur les intérêts des utilisateurs, et la sécurisation des accès. Le traitement des données collectées par ces moyens vise à enrichir l'expérience utilisateur, à fournir des contenus et des publicités personnalisés, et à garantir l'intégrité et la sécurité des services proposés.

<div id='section-id-719'/>

### 27.3. Gestion du consentement pour les cookies

§ Le consentement de l'utilisateur constitue la base légale principale permettant l'utilisation de cookies et technologies similaires non strictement nécessaires au fonctionnement des services. Le Responsable du Traitement s'assure que les utilisateurs sont clairement informés de l'utilisation de ces technologies dès leur première visite sur les plateformes numériques, leur offrant la possibilité d'accepter ou de refuser l'utilisation des cookies, sauf pour ceux essentiels à la prestation des services. Un mécanisme facile d'accès et d'utilisation permet aux utilisateurs de modifier leurs préférences à tout moment.

Pour une explication détaillée sur les types de cookies utilisés, les finalités spécifiques de chaque catégorie de cookie, ainsi que les modalités pour gérer votre consentement ou retirer votre consentement précédemment donné, veuillez vous référer à la [politique détaillée sur les cookies et technologies similaires](/legal/cookies) de notre site. Cette politique complémentaire vise à offrir une transparence totale sur notre approche de la collecte et du traitement des données à travers l'usage de ces technologies et à assurer le respect de vos droits en tant qu'utilisateur conformément aux dispositions légales en vigueur.

Le Responsable du Traitement s'engage à respecter scrupuleusement les préférences exprimées par les utilisateurs concernant l'utilisation des cookies et technologies similaires, garantissant ainsi une approche respectueuse de la vie privée et conforme aux exigences réglementaires actuelles.

<div id='section-id-727'/>

# Modifications de la Politique de Confidentialité

<div id='section-id-729'/>

## 28. Processus de mise à jour

En vertu des obligations imposées par le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi qu'en conformité avec les législations nationales applicables en matière de protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") se réserve le droit de modifier la présente politique de confidentialité à tout moment, en fonction des évolutions législatives, réglementaires, jurisprudentielles, ou technologiques susceptibles d'affecter les pratiques de traitement des données à caractère personnel.

<div id='section-id-733'/>

### 28.1. Engagement de Notification

§ Le Responsable du Traitement s'engage à informer les utilisateurs de ses services, de manière claire et visible, de toute modification substantielle apportée à la présente politique de confidentialité. Cette notification peut prendre diverses formes, incluant, sans s'y limiter, la publication d'un avis sur le site Internet du Responsable du Traitement, l'envoi d'une communication électronique aux utilisateurs disposant d'un compte actif, ou toute autre méthode jugée appropriée pour garantir une prise de connaissance effective par les personnes concernées.

<div id='section-id-737'/>

### 28.2. Contenu des Modifications

§ Toute modification apportée à la présente politique de confidentialité reflétera les changements dans les pratiques de traitement des données à caractère personnel du Responsable du Traitement, les ajustements nécessaires pour se conformer aux évolutions légales et réglementaires, ou les améliorations apportées pour renforcer la protection des données personnelles des utilisateurs. Le contenu de la modification précisera les nouvelles dispositions, leur date d'entrée en vigueur, ainsi que les implications pratiques pour les utilisateurs.

<div id='section-id-741'/>

### 28.3. Accès à la Politique Révisée

§ La politique de confidentialité révisée sera accessible en permanence sur le site Internet du Responsable du Traitement, dans une section spécifiquement dédiée. Les utilisateurs seront encouragés à consulter régulièrement cette politique afin de se tenir informés des dernières mises à jour et de comprendre comment leurs données à caractère personnel sont traitées.

<div id='section-id-745'/>

### 28.4. Droits des Utilisateurs

§ Le Responsable du Traitement garantit que les modifications de la politique de confidentialité ne porteront en aucun cas atteinte aux droits des utilisateurs tels que consacrés par le RGPD et les législations nationales. Les utilisateurs disposent du droit de s'opposer à certaines formes de traitement, de retirer leur consentement à tout moment, et d'exercer les droits d'accès, de rectification, d'effacement, de limitation du traitement, de portabilité des données, et d'opposition conformément aux modalités précédemment établies.

Le Responsable du Traitement s'engage à maintenir un haut niveau de transparence et de conformité en matière de protection des données à caractère personnel, veillant à ce que toute modification de la politique de confidentialité soit effectuée dans le respect total des obligations légales et réglementaires, et dans l'intérêt de la protection des droits et libertés des personnes concernées.

<div id='section-id-751'/>

## 29. Notification des modifications

Conformément aux exigences statutaires émanant du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), et dans le respect des législations nationales pertinentes en matière de protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") s'oblige à notifier les utilisateurs de ses services de toute modification apportée à la présente politique de confidentialité qui pourrait impacter le traitement de leurs données à caractère personnel ou leurs droits relatifs à ces données.

<div id='section-id-755'/>

### 29.1. Procédure de Notification

- **Méthodes de notification** : Le Responsable du Traitement utilise des moyens de communication efficaces pour informer les utilisateurs des modifications de la politique de confidentialité, y compris, sans limitation, la publication d'un avis distinct et bien visible sur le site web de l'entité, l'envoi d'une notification par courrier électronique aux utilisateurs ayant fourni une adresse électronique valide, ou toute autre méthode assurant une information adéquate et en temps opportun.
- **Délai de notification** : Les utilisateurs seront notifiés des modifications substantielles de la politique de confidentialité avant leur entrée en vigueur, offrant ainsi un délai raisonnable pour examiner les modifications et, si nécessaire, prendre les mesures appropriées.
- **Contenu de la notification** : La notification comprendra un résumé des modifications importantes apportées à la politique, leur impact potentiel sur le traitement des données à caractère personnel des utilisateurs, ainsi que les mesures que les utilisateurs peuvent envisager s'ils ne souhaitent pas accepter ces modifications. Le texte intégral de la politique révisée sera également mis à disposition, avec une indication claire des sections modifiées ou ajoutées.
- **Accès continu à la politique révisée** : La version la plus récente de la politique de confidentialité sera accessible en permanence sur le site web du Responsable du Traitement, permettant aux utilisateurs de consulter à tout moment les termes actuellement en vigueur.

<div id='section-id-762'/>

### 29.2. Droits des Utilisateurs en cas de modification

§ Le Responsable du Traitement rappelle aux utilisateurs que toute modification de la politique de confidentialité n'affecte en rien leurs droits en matière de protection des données à caractère personnel tels que garantis par le RGPD et les législations nationales. Les utilisateurs conservent le droit de retirer leur consentement au traitement de leurs données, de s'opposer à certaines formes de traitement, et d'exercer leurs droits d'accès, de rectification, d'effacement, de limitation du traitement, et de portabilité des données.

Le Responsable du Traitement s'engage à maintenir une communication transparente et ouverte avec les utilisateurs concernant toute modification de la politique de confidentialité, garantissant que les intérêts et les droits des utilisateurs en matière de protection des données à caractère personnel sont préservés et respectés conformément aux normes les plus élevées imposées par le droit applicable.

<div id='section-id-768'/>

# Dispositions Finales

<div id='section-id-770'/>

## 30. Juridiction compétente et Loi applicable

Conformément aux obligations légales émanant du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi qu'en vertu des dispositions des législations nationales en vigueur en matière de protection des données personnelles, les présentes dispositions finales déterminent la juridiction compétente et la loi applicable à la présente politique de confidentialité et à tout litige ou réclamation découlant de, ou en relation avec celle-ci, sans préjudice des droits conférés aux personnes concernées par le RGPD et les législations nationales.

<div id='section-id-774'/>

### 30.1. Loi Applicable

§ La présente politique de confidentialité et toutes les questions relatives au traitement des données à caractère personnel par Colabl France (ci-après désigné le "Responsable du Traitement") sont régies par la loi du pays où le Responsable du Traitement est établi, dans la mesure où celle-ci ne contrevient pas aux dispositions impératives du RGPD ou aux dispositions protectrices des législations nationales en vigueur dans les pays des personnes concernées.

<div id='section-id-778'/>

### 30.2. Juridiction compétente

§ Tout litige découlant de, ou en relation avec, la présente politique de confidentialité, y compris ceux concernant son existence, sa validité, son interprétation, son exécution ou sa résiliation, ainsi que tout litige relatif au traitement des données à caractère personnel, sera soumis à la juridiction exclusive des tribunaux du pays où le Responsable du Traitement est établi, sous réserve des droits conférés aux personnes concernées de saisir l'autorité de contrôle compétente et/ou un tribunal dans leur État membre de résidence habituelle, conformément aux dispositions du RGPD.

<div id='section-id-782'/>

### 30.3. Recours et Voies de droit

§ Les personnes concernées disposent du droit d'introduire une réclamation auprès de l'autorité de contrôle compétente dans leur pays de résidence habituelle, de leur lieu de travail, ou du lieu de l'infraction présumée au RGPD, si elles estiment que le traitement de leurs données à caractère personnel constitue une violation des dispositions réglementaires applicables. Elles ont également le droit de saisir les tribunaux compétents pour toute réclamation relative à leurs droits en vertu du RGPD et des législations nationales applicables.

Le Responsable du Traitement s'engage à coopérer pleinement avec les autorités de contrôle et à fournir toute assistance nécessaire pour faciliter la résolution rapide et équitable de toute réclamation ou litige concernant le traitement des données à caractère personnel, dans le plein respect des droits des personnes concernées et des obligations légales applicables.

<div id='section-id-788'/>

## 31. Voies de recours en cas de litige

Dans le cadre de l'adhésion aux obligations légales découlant du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), et conformément aux législations nationales en vigueur relatives à la protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") établit les voies de recours disponibles pour les personnes concernées en cas de litige relatif à la présente politique de confidentialité ou au traitement de leurs données à caractère personnel.

<div id='section-id-792'/>

### 31.1. Recours Administratif

- **Autorité de contrôle** : Les personnes concernées ont le droit d'introduire une réclamation auprès de l'autorité de contrôle compétente dans leur État membre de résidence habituelle, de leur lieu de travail, ou du lieu où l'infraction au RGPD est présumée avoir eu lieu, si elles considèrent que le traitement de leurs données à caractère personnel constitue une violation des dispositions du RGPD.
- **Procédure** : La réclamation doit être soumise par écrit, en détaillant la nature du litige, les faits et les preuves à l'appui, et les dommages subis. L'autorité de contrôle informera le plaignant des avancées et de l'issue de la réclamation, y compris de la possibilité de recourir à une voie de recours judiciaire.

<div id='section-id-797'/>

### 31.2. Recours Judiciaire

- **Juridiction compétente** : En complément des recours administratifs, les personnes concernées ont le droit de saisir la juridiction compétente dans leur État membre de résidence habituelle ou dans l'État membre où le Responsable du Traitement est établi, pour toute réclamation découlant du traitement de leurs données à caractère personnel en violation du RGPD.
- **Procédure et Effet** : La procédure judiciaire est initiée conformément aux règles de procédure civile de l'État membre concerné. Le tribunal compétent peut ordonner au Responsable du Traitement de prendre des mesures spécifiques pour remédier à la violation.

<div id='section-id-802'/>

### 31.3. Mécanismes de Médiation et d'Arbitrage

§ Le Responsable du Traitement encourage les personnes concernées à prendre contact directement avec son service de gestion des réclamations ou son Délégué à la Protection des Données pour résoudre amiablement tout différend ou malentendu relatif au traitement de leurs données à caractère personnel avant de recourir à des voies de recours administratives ou judiciaires.

Le Responsable du Traitement s'engage à coopérer pleinement avec les autorités de contrôle et les instances judiciaires compétentes pour assurer une résolution rapide et équitable des litiges, dans le plein respect des droits des personnes concernées et des dispositions légales et réglementaires en vigueur.

<div id='section-id-808'/>

# Annexes

<div id='section-id-810'/>

## 32. Glossaire des Termes Techniques et Juridiques

Dans le cadre de la présente politique de confidentialité, et afin de faciliter la compréhension de ses dispositions par les personnes concernées, Colabl France (ci-après désigné le "Responsable du Traitement") fournit un glossaire des termes techniques et juridiques fréquemment utilisés en matière de protection des données à caractère personnel, conformément au Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi qu'aux législations nationales applicables.

- **Données à caractère personnel** : Toute information se rapportant à une personne physique identifiée ou identifiable ; une personne identifiable étant une personne qui peut être identifiée, directement ou indirectement, notamment par référence à un identifiant tel qu'un nom, un numéro d'identification, des données de localisation, un identifiant en ligne, ou à un ou plusieurs éléments spécifiques propres à son identité physique, physiologique, génétique, psychique, économique, culturelle ou sociale.
- **Traitement** : Toute opération ou ensemble d'opérations effectuées sur des données à caractère personnel ou sur des ensembles de données à caractère personnel, que ce soit par des moyens automatisés ou non, telles que la collecte, l'enregistrement, l'organisation, la structuration, la conservation, l'adaptation ou la modification, l'extraction, la consultation, l'utilisation, la divulgation par transmission, la diffusion ou toute autre forme de mise à disposition, le rapprochement ou l'interconnexion, la limitation, l'effacement ou la destruction.
- **Consentement** : Toute manifestation de volonté libre, spécifique, éclairée et univoque par laquelle la personne concernée accepte, par une déclaration ou par un acte positif clair, que des données à caractère personnel la concernant fassent l'objet d'un traitement.
- **Responsable du Traitement** : La personne physique ou morale, l'autorité publique, le service ou tout autre organisme qui, seul ou conjointement avec d'autres, détermine les finalités et les moyens du traitement des données à caractère personnel.
- **Sous-traitant** : La personne physique ou morale, l'autorité publique, le service ou tout autre organisme qui traite des données à caractère personnel pour le compte du responsable du traitement.
- **Autorité de contrôle** : Une autorité publique indépendante établie par un État membre en vertu de l'article 51 du RGPD, chargée de surveiller l'application du règlement afin de protéger les libertés et droits fondamentaux des personnes physiques en ce qui concerne le traitement et de faciliter la libre circulation de ces données au sein de l'Union européenne.
- **Violation de données à caractère personnel** : Une violation de la sécurité entraînant, de manière accidentelle ou illicite, la destruction, la perte, l'altération, la divulgation non autorisée de, ou l'accès non autorisé à des données à caractère personnel transmises, conservées ou traitées d'une autre manière.

Ce glossaire est destiné à éclairer les termes utilisés dans la présente politique de confidentialité et ne prétend pas à l'exhaustivité. Pour toute question relative à la compréhension de ces termes ou à toute autre disposition de la politique de confidentialité, les personnes concernées sont invitées à contacter le Délégué à la Protection des Données du Responsable du Traitement.


<div id='section-id-825'/>

## 33. Modèles de formulaire d'exercice des droits par les utilisateurs

En vue de faciliter l'exercice des droits conférés par le Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données (ci-après désigné "Règlement Général sur la Protection des Données" ou "RGPD"), ainsi que par les législations nationales applicables en matière de protection des données personnelles, Colabl France (ci-après désigné le "Responsable du Traitement") propose ci-après des modèles de formulaire pour l'exercice des droits des utilisateurs. Ces formulaires sont conçus pour assurer que les demandes soient traitées de manière efficace et conformes aux exigences légales.

<div id='section-id-829'/>

### 33.1. Modèle de Formulaire pour le Droit d'Accès

```
À l'attention du Délégué à la Protection des Données
Colabl France, RGPD/DPO - FRETAY Adrian
[ADRESSE_POSTALE_SIEGE_COLABL]
[EMAIL_RGPD_COLABL]


Je soussigné(e) [Nom et Prénom], demeurant [Adresse], sollicite par la présente l'exercice de mon droit d'accès conformément à l'article 15 du RGPD.

Je souhaite obtenir les informations suivantes :
- La confirmation que des données à caractère personnel me concernant sont ou ne sont pas traitées ;
- Les finalités du traitement ;
- Les catégories de données à caractère personnel concernées ;
- Les destinataires ou catégories de destinataires auxquels mes données ont été ou seront communiquées ;
- La durée de conservation des données à caractère personnel ;
- Les droits de rectification, d'effacement et de limitation concernant mes données personnelles.

Je joins à cette demande une copie de mon document d'identité afin de prouver mon identité.

Signature :
[Signature de l'Utilisateur]
```

<div id='section-id-854'/>

### 33.2. Modèle de Formulaire pour le Droit de Rectification

```
À l'attention du Délégué à la Protection des Données
Colabl France, RGPD/DPO - FRETAY Adrian
[ADRESSE_POSTALE_SIEGE_COLABL]
[EMAIL_RGPD_COLABL]

Je soussigné(e) [Nom et Prénom], demeurant [Adresse], sollicite par la présente l'exercice de mon droit de rectification conformément à l'article 16 du RGPD.

Je souhaite que les données à caractère personnel suivantes soient corrigées / complétées :
- [Description précise des données à corriger et de la correction souhaitée]

Je joins à cette demande une copie de mon document d'identité afin de prouver mon identité.

Signature :
[Signature de l'Utilisateur]
```
