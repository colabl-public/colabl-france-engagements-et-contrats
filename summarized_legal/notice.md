#Summary 

  


_Dernière mise à jour le 26/09/2023_

Ce site est édité par la société **Colabl France** - Société par actions simplifiée au capital de 10 000 euros, immatriculée au Registre du commerce et des sociétés (“RCS”) de Saint-Etienne le 23/06/2023 sous le numéro **953 740 743**.

> **Colabl France SAS** \
> Siège social : [ADRESSE_POSTALE_SIEGE_COLABL] \
> N°TVA : [N_TVA_COLABL] \
> Téléphone : [NUMERO_TEL_STANDARD_COLABL] \
> Email : [EMAIL_CONTACT_COLABL] \
> Directeur de la publication : [DIRECTEUR_PUBLICATION_COLABL] \

**Ce site est hébergé par la société**

> **OVH SAS** \
> SAS au capital de 10 174 560 € \
> RCS Lille Métropole 424 761 419 00045 \
> Code APE 2620Z \
> N° TVA : FR 22 424 761 419 \
> Siège social : 2 rue Kellermann - 59100 Roubaix - France \
> _OVH SAS est une filiale de la société OVH Groupe SA, société immatriculée au RCS de Lille sous le numéro 537 407 926 sise 2, rue Kellermann, 59100 Roubaix._ \
> Directeur de la publication d'OVH : Michel Paulin
