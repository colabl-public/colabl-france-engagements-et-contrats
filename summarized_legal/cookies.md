#Summary 

- [**Présentation**](#section-id-5)
  - [**Cookies**](#section-id-18)
    - [**Catégories de cookies**](#section-id-24)
      - [1. **Strictement nécessaire**](#section-id-29)
      - [2. **Performance**](#section-id-40)
      - [3. **Préférences**](#section-id-51)
      - [4. **Publicité**](#section-id-64)
      - [5. **Kits de développement logiciel (“SDK”)**](#section-id-81)
    - [**Gestion des Cookies**](#section-id-92)
      - [**Transfert de données**](#section-id-98)
      - [**Conservation**](#section-id-102)
      - [**Suppression des Cookies**](#section-id-108)
        - [**Étape 1 : Ouvrir votre Navigateur Web**](#section-id-112)
        - [**Étape 2 : Accéder aux Paramètres de votre Navigateur**](#section-id-116)
        - [**Étape 3 : Accéder à l'Historique des Cookies**](#section-id-128)
        - [**Étape 4 : Sélectionner les Options de Suppression**](#section-id-143)
        - [**Étape 5 : Confirmation et Attente**](#section-id-160)
        - [**Étape 6: Redémarrage du Navigateur**](#section-id-164)
  


Dernière mise à jour le 26/09/2023

La présente politique (“Politique”) relative aux cookies s’applique aux entités juridiques de Colabl France SAS qui utilisent les noms de marque Colabl, Colabl Studio, Colabl AI, ainsi qu’aux entités suivantes : TimeBlue, Prism. La présente Politique explique comment nous utilisons les cookies et technologies de suivi similaires lorsqu’un utilisateur interagit avec nous en ligne via nos sites Web, Applications mobiles, Logiciels et services numériques (collectivement, les Sites et Applications mobiles et Logiciels).

<div id='section-id-5'/>

# **Présentation**

Nous avons élaboré la présente Politique pour :

1. vous fournir des informations générales sur les cookies et autres technologies de suivi que nous appellerons “Cookies” ;
2. identifier les finalités spécifiques du traitement des données à caractère personnel collecté à partir des cookies installés sur nos Sites, Applications mobiles et Logiciels ;
3. vous expliquer si et comment vous pouvez gérer les paramètres des cookies sur nos Sites, Applications mobiles et Logiciels ;
4. vous informer sur la période de conservation des données à caractère personnel collectées via nos Sites, Applications mobiles et Logiciels.

La présente Politique doit être lue conjointement avec les autres avis de confidentialité et de protection des données pertinents fournis par Colabl France SAS. Veuillez consulter ces avis pour obtenir des informations supplémentaires sur nos pratiques de confidentialité applicables à votre produit ou service, y compris, mais sans s’y limiter, nos clients, professionnels, associés et sous-traitants :

* OVH

<div id='section-id-18'/>

## **Cookies**

Un cookie est un petit fichier contenant certaines données qui sont créées et stockées lorsque vous visitez un site Web à l’aide d’un ordinateur ou d’un appareil mobile. Lorsque vous visitez un site Web, un cookie peut être utilisé pour suivre les activités de votre navigateur et vous fournir une expérience en ligne plus cohérente et plus efficace.

Nous pouvons collecter certaines informations vous concernant par le biais de notre utilisation de cookies, de kits de développement logiciel (SDK) et d’autres technologies de suivi lorsque vous utilisez nos Sites, Applications mobile et Logiciels. Par exemple, nous identifions et stockons la région où vous vous trouvez en fonction de votre adresse IP partiellement masquée (qui est anonymisée immédiatement par la suite) pour vous diriger vers la page Web appropriée spécifique à chaque pays.

<div id='section-id-24'/>

### **Catégories de cookies**

La présente déclaration vise à informer de manière claire et précise les utilisateurs de nos sites, applications mobiles et logiciels concernant les différents types de cookies que nous utilisons, leur finalité et leur impact sur la vie privée. Nous fournissons également des exemples courants pour une meilleure compréhension.


<div id='section-id-29'/>

#### 1. **Strictement nécessaire**

Les cookies strictement nécessaires sont essentiels au fonctionnement de nos sites, applications mobiles et logiciels. Ils ne peuvent pas être désactivés dans nos systèmes. Ils sont déployés en réponse aux actions des utilisateurs, telles que la configuration des préférences de confidentialité.

Exemples courants :

- Cookies empêchant le dysfonctionnement des sites, applications mobiles et logiciels.
- Équilibrage de charge pour une distribution efficace du trafic sur nos serveurs.
- Maintien de la connexion des utilisateurs pendant leur navigation à l'aide de cookies de session.
    
    
<div id='section-id-40'/>

#### 2. **Performance**

Les cookies de performance, également appelés cookies d'exécution ou d'analyse, nous permettent de mesurer et d'améliorer l'exécution des tâches sur nos plateformes. Les données collectées sont agrégées et anonymisées, et ne permettent pas d'identifier un utilisateur spécifique.

Exemples courants :

- Collecte de données agrégées et anonymisées pour améliorer les fonctionnalités.
- Mesure du nombre de visiteurs uniques.
- Activation de la fonctionnalité de chat vocal-texte.
- Balises de suivi enregistrant les comportements des visiteurs sur nos sites et applications.

<div id='section-id-51'/>

#### 3. **Préférences**

Les cookies de préférence permettent à nos sites, applications mobiles et logiciels d'offrir des fonctionnalités améliorées et une personnalisation accrue en enregistrant les choix des utilisateurs.

Exemples courants :

- Mémorisation des choix effectués lors de visites précédentes.
- Configuration des préférences telles que la confidentialité, la langue, la police, la taille du texte, et les pré-remplissages de texte.
- Utilisation de fonctionnalités telles que la vidéo ou les commentaires sur les blogs.
- Activation du chat en direct, y compris la conversion vocale en texte.
- Géolocalisation pour une détermination temporaire de l'emplacement géographique.


<div id='section-id-64'/>

#### 4. **Publicité**

Les cookies et technologies publicitaires sont utilisés pour diffuser des publicités susceptibles d'intéresser les utilisateurs en fonction de leurs centres d'intérêt. Certaines informations peuvent être partagées avec des tiers, tels que des annonceurs.

Exemples courants :

- Pixels marketing collectant des données de navigation et d'interaction avec des annonces.
- Utilisation de cookies par des partenaires publicitaires pour rappeler les visites précédentes.
- Suivi de l'activité en ligne pour des publicités plus pertinentes, limitant le nombre d'affichages publicitaires.
- Mesure de l'efficacité des campagnes publicitaires.
- Interactions avec les plateformes de réseaux sociaux.

En conformité avec les directives émises par l'Autorité de protection des données d'un pays, relatives à l'interdiction d'utiliser Google Analytics, il est impératif de noter que les cookies associés à Google Analytics ne seront ni chargés, ni susceptibles d'être acceptés par notre système dès lors qu'il identifie un utilisateur accédant à nos Sites, Applications mobiles et 

Logiciels à partir d'une adresse IP localisée dans ledit pays. Cette mesure est mise en place dans le strict respect desdites directives et de notre engagement à respecter les dispositions légales en vigueur en matière de protection des données.


<div id='section-id-81'/>

#### 5. **Kits de développement logiciel (“SDK”)**

Les kits de développement logiciel (SDK) sont des ensembles d'outils spécifiques aux appareils mobiles permettant aux développeurs de créer des applications personnalisées pour une expérience mobile enrichie.

Types de SDK :

- SDK MacOS X
- SDK Windows 7
- Kit de développement Java (JDK)
- SDK iPhone

<div id='section-id-92'/>

### **Gestion des Cookies**

Nous observons strictement les exigences légales de chaque juridiction et nous sommes en mesure de désactiver certaines technologies de cookies. Vous avez également la possibilité d'accepter ou de refuser certains types de cookies grâce à une bannière de consentement. Toutefois, veuillez noter que la disponibilité et le fonctionnement de notre bannière de consentement peuvent varier en fonction de la juridiction à partir de laquelle vous accédez à nos Sites, Applications mobiles ou Logiciels. En cas de suppression de cookies, de connexion à nos plateformes depuis un autre appareil ou de changement de navigateur, il peut être nécessaire de répéter vos préférences en matière de cookies.

Lorsque nous concluons des accords contractuels avec des tiers pour la gestion du contenu de nos Sites, Applications mobiles et Logiciels, ces tiers sont contractuellement tenus de n'utiliser vos informations que conformément à nos instructions spécifiques. De plus, ils sont tenus de mettre en place des mesures de sécurité et de confidentialité appropriées pour protéger vos données. Si vous quittez l'un de nos sites, applications mobiles ou logiciels, vous comprenez que nous n'exerçons aucun contrôle sur le site auquel vous accédez. Il convient de noter que le tiers responsable des cookies, du suivi des cookies et de la gestion des cookies sur son site est le seul responsable de ces aspects.

<div id='section-id-98'/>

#### **Transfert de données**

Les données personnelles des utilisateurs visitant nos Sites, Applications mobiles, Logiciels et/ou utilisant nos services peuvent être transférées ou traitées dans tout pays où nous avons des installations ou engageons des prestataires de services, y compris en France. Ces transferts sont effectués en stricte conformité avec la législation applicable en matière de protection des données et conformément à nos politiques de confidentialité.

<div id='section-id-102'/>

#### **Conservation**

Il est important de noter que les cookies ont une durée de vie variable. Certains sont temporaires (cookies de session), tandis que d'autres peuvent persister sur votre appareil jusqu'à ce que vous les supprimiez manuellement ou jusqu'à ce que votre navigateur les supprime conformément à la durée de vie définie dans le cookie (cookie persistant).

Nous conservons les données personnelles collectées via les cookies aussi longtemps que nécessaire ou autorisé par la finalité pour laquelle elles ont été collectées. Les critères pris en compte pour déterminer nos délais de conservation comprennent la raison initiale de la collecte des données personnelles, les obligations légales qui nous incombent, ainsi que les considérations liées à notre situation juridique, telles que les délais de prescription applicables, les litiges en cours ou les enquêtes réglementaires.

<div id='section-id-108'/>

#### **Suppression des Cookies**

La suppression des cookies de votre navigateur Internet peut améliorer votre confidentialité en ligne et résoudre certains problèmes de navigation. Cette procédure vous guidera à travers les étapes pour supprimer les cookies sur les navigateurs les plus courants, tels que Google Chrome, Mozilla Firefox et Microsoft Edge. Vous n'avez pas besoin de compétences techniques particulières pour suivre cette procédure.

<div id='section-id-112'/>

##### **Étape 1 : Ouvrir votre Navigateur Web**

Allumez votre ordinateur et lancez le navigateur Internet que vous utilisez habituellement, comme Google Chrome, Mozilla Firefox ou Microsoft Edge.

<div id='section-id-116'/>

##### **Étape 2 : Accéder aux Paramètres de votre Navigateur**

1. Pour **Google Chrome**
* Cliquez sur les trois points verticaux en haut à droite de la fenêtre du navigateur (ce sont les "Paramètres").
* Sélectionnez "Paramètres" dans le menu déroulant.
2. Pour **Mozilla Firefox**
* Cliquez sur les trois lignes horizontales en haut à droite de la fenêtre du navigateur (ce sont les "Options").
* Sélectionnez "Options" dans le menu.
3. Pour **Microsoft Edge**
* Cliquez sur les trois points horizontaux en haut à droite de la fenêtre du navigateur (ce sont les "Paramètres et plus").
* Sélectionnez "Paramètres" dans le menu.

<div id='section-id-128'/>

##### **Étape 3 : Accéder à l'Historique des Cookies**



1. Pour **Google Chrome**
* Faites défiler vers le bas jusqu'à trouver "Effacer les données de navigation".
* Cliquez dessus.
2. Pour **Mozilla Firefox**
* Dans le menu de gauche, sélectionnez "Vie privée et sécurité".
* Faites défiler jusqu'à la section "Cookies et données de site".
* Cliquez sur "Effacer les données…" sous cette section
3. Pour **Microsoft Edge**
* Dans le menu de gauche, sélectionnez "Confidentialité, recherche et services".
* Sous "Effacer les données de navigation", cliquez sur "Choisir les données à effacer".

<div id='section-id-143'/>

##### **Étape 4 : Sélectionner les Options de Suppression**



1. Pour **Google Chrome**
* Assurez-vous que "Cookies et autres données de site" est coché.
* Vous pouvez également sélectionner d'autres éléments à supprimer si vous le souhaitez.
* Cliquez sur "Effacer les données".
2. Pour **Mozilla Firefox**
* Assurez-vous que "Cookies" est coché.
* Vous pouvez également sélectionner d'autres éléments à supprimer si vous le souhaitez.
* Cliquez sur "Effacer".
3. Pour **Microsoft Edge**
* Assurez-vous que "Cookies et données de site" est coché.
* Vous pouvez également sélectionner d'autres éléments à supprimer si vous le souhaitez.
* Cliquez sur "Effacer".

<div id='section-id-160'/>

##### **Étape 5 : Confirmation et Attente**

Une boîte de confirmation apparaîtra pour vous demander de confirmer la suppression des cookies. Cliquez sur "Effacer" ou "Confirmer" pour lancer le processus. Attendez que le navigateur termine la suppression des cookies.

<div id='section-id-164'/>

##### **Étape 6: Redémarrage du Navigateur**

Fermez complètement votre navigateur, puis rouvrez-le.

A l'issue de cette procédure, vous avez réussi à supprimer les cookies de votre navigateur. Votre navigation Internet devrait maintenant être plus propre et plus sécurisée. Vous pouvez répéter ces étapes chaque fois que vous le souhaitez pour maintenir la confidentialité de votre expérience en ligne.


---

La présente Politique relative aux cookies est sujette à des modifications périodiques. Toute modification sera publiée sur cette page, accompagnée d'une nouvelle date de "Dernière mise à jour".
